package almogaz.orangestudio.com.View.Activities;

import android.app.Activity;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.R;
import uk.co.senab.photoview.PhotoViewAttacher;

public class Caricature_Image extends Activity implements PhotoViewAttacher.OnViewTapListener {
    Bundle extras;
    Constants constants;
    int i=0;
    AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_caricature__image);

        extras = getIntent().getExtras();
        constants=new Constants(this);
        appBarLayout=(AppBarLayout)findViewById(R.id.appbar);

        // we can be in one of these 3 states


        final ImageView imageview = (ImageView) findViewById(R.id.image_view);

        Picasso.with(getApplicationContext())
                .load(extras.getString("object_key_image"))
                .placeholder(R.drawable.bg_white_black)
                .into(imageview);


        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(i==0){
                    appBarLayout.setVisibility(View.INVISIBLE);
                    i=1;
                }
                else{
                    appBarLayout.setVisibility(View.VISIBLE);
                    i=0;
                }
            }
        });


        PhotoViewAttacher photoViewAttacher=new PhotoViewAttacher(imageview);
        photoViewAttacher.setOnViewTapListener(this);
        photoViewAttacher.update();

        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton btn_share = (ImageButton) findViewById(R.id.btn_share);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constants.shareNews(extras.getString("object_key_title"),extras.getString("object_key_url"));
            }
        });
    }

    public void onViewTap(View view, float x, float y) {
        if(i==0){
            appBarLayout.setVisibility(View.INVISIBLE);
            i=1;
        }
        else{
            appBarLayout.setVisibility(View.VISIBLE);
            i=0;
        }
    }

}
