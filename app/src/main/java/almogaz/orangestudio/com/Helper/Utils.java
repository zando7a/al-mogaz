package almogaz.orangestudio.com.Helper;

/**
 * Created by ahalima on 19/04/17.
 */

import android.app.Activity;

import almogaz.orangestudio.com.R;

public class Utils
{
    public static int sTheme;
    public static int i=0;
    public static String background,fontcolor;
    /**
     * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
     */
    public static void changeToTheme(Activity activity, int theme)
    {
        sTheme = theme;
        activity.recreate();
//        activity.startActivity(new Intent(activity, activity.getClass()));
//        activity.finish();

    }
    /** Set the theme of the activity, according to the configuration. */
    public static void onActivityCreateSetTheme(Activity activity)
    {
        switch (sTheme)
        {
            default:
            case 0:
                activity.setTheme(R.style.AppTheme);
                background= "#FFFFFF";
                fontcolor="#000000";
                break;
            case 1:
                activity.setTheme(R.style.AppTheme1);
                background="#000000";
                fontcolor="#FFFFFF";
                break;
            case 2:
                activity.setTheme(R.style.AppTheme2);
                background="#461022";
                fontcolor="#07fa82";
                break;
        }
    }
}
