package almogaz.orangestudio.com.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.tapadoo.alerter.Alerter;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Controller.NewsAdapter;
import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Activities.OneCategoryNewsShow;
import almogaz.orangestudio.com.View.Activities.Setting;


public class Videos extends Fragment{

    Constants constants;
    int page_num = 0;
    ArrayList<News> newses = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    GeometricProgressView loader;
    RecyclerView rv_videos;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    NewsAdapter newsAdapter;
    SwipeRefreshLayout refreshLayout ;
    public static int SHOW_TYPE =1;
    RecyclerView rv_news;
    int mode;

    public Videos() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static Videos newInstance(String text) {
        Videos fragment = new Videos();
        Bundle b = new Bundle();
        b.putString("msg", text);

        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        constants = new Constants(getActivity());
        mode = SettingsAdapter.mode;

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                newses.clear();
                page_num =0;
                newsAdapter = new NewsAdapter(newses, getContext(),SHOW_TYPE);
                rv_news.setAdapter(newsAdapter);
                makeJsonArrayRequest();
            }
        });

        rv_news = (RecyclerView) view.findViewById(R.id.rv_news);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_news.setLayoutManager(mLayoutManager);
        rv_news.setHasFixedSize(true);
        loader = (GeometricProgressView) view.findViewById(R.id.progressView);
        loader.setNumberOfAngles(16);
        loader.setColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));
        newsAdapter = new NewsAdapter(newses, getContext(),SHOW_TYPE);
        rv_news.setAdapter(newsAdapter);

        BottomNavigationView navigationView = (BottomNavigationView) view.findViewById(R.id.navigation);
        //-----------*---------
        if(mode == 0){
            navigationView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.background_material_light));
        }
        else if(mode == 1){
            navigationView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.background_material_light1));
        }
        else {
            navigationView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.background_material_light2));
        }


        makeJsonArrayRequest();
        if (constants.isNetworkAvailable()){
            makeJsonArrayRequest();
        }
        else {
            Alerter.create(getActivity())
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .show();
            makeJsonArrayRequest();
        }

        rv_news.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            page_num++;
                            makeJsonArrayRequest();

                            if (constants.isNetworkAvailable()){
                                loading = false;
                                page_num ++;
                                makeJsonArrayRequest();
                            }
                            else {
                                Alerter.create(getActivity())
                                        .setTitle(R.string.no_internet_connection)
                                        .setText(R.string.no_internet_connection_detail)
                                        .setBackgroundColor(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_signal_wifi_off)
                                        .show();
                                makeJsonArrayRequest();
                            }
                        }
                    }
                }
            }
        });

        return view ;
    }

    private void makeJsonArrayRequest() {
        if (constants.isNetworkAvailable()) {
            RequestQueue queue = Volley.newRequestQueue(getContext());
            queue.getCache().clear();
        } else {
            Alerter.create(getActivity())
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        JsonArrayRequest req = new JsonArrayRequest("http://almogaz.com/api/get_one_category?tid=2&page="+page_num,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("", response.toString());
                        try {
                            News saved_news;
                            int news_status = 0;
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject news = (JSONObject) response
                                        .get(i);

                                if (constants.checkFav(Integer.parseInt(news.getString("news_id")))){
                                    news_status=1;
                                }
                                else {
                                    news_status =0;
                                }

                                if (news.isNull("views_num")) {
                                    saved_news = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "لا يوجد مشاهدات", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                } else {
                                    saved_news = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "2222", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                }

                                if (((i+1)%6)==0){
                                    newses.add(new News("adsPartItem_fb","","","","","","","","","",0));
                                }
                                else if (((i+1)%3)==0){
                                    newses.add(new News("adsPartItem_google","","","","","","","","","",0));
                                }
                            }
                            loading = true;
                            rv_news.setVisibility(View.VISIBLE);
                            newsAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        loader.setVisibility(View.GONE);
                        refreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
//                Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                loader.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
            }
        }){

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(50000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        if (constants.isNetworkAvailable()) {
            req.setShouldCache(false);
        } else {
            Alerter.create(getActivity())
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        req.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(req);
    }
}
