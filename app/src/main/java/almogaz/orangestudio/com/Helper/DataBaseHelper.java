package almogaz.orangestudio.com.Helper;

/**
 * Created by Same7-Orange on 1/4/2018.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import almogaz.orangestudio.com.Model.News;

/**
 * Created by sameh on 02/08/17.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String db_name = "Almogaz_Fav_News.db";
    Context context ;

    public DataBaseHelper(Context context) {
        super(context, db_name, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
//        this.context=context;
    }

//     ------------- creating database tables --------------

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS fav_news (ID INTEGER PRIMARY KEY, news_title TEXT, news_thumb TEXT , news_date TEXT ,  views_num TEXT  , page_url TEXT , news_detail TEXT  , news_type TEXT, news_status INTEGER)");
    }

//    ---------method for insert data in countries table --------------

    public boolean allNewsData(News news) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ID", Integer.parseInt(news.getNews_id()));
        contentValues.put("news_title", news.getNews_title());
        contentValues.put("news_thumb", news.getNews_thumb());
        contentValues.put("news_date", ""+news.getNews_date());
        contentValues.put("views_num", ""+news.getViews_num());
        contentValues.put("page_url", ""+news.getPage_url());
        contentValues.put("news_detail", ""+news.getNews_detail());
        contentValues.put("news_type", ""+news.getType());
        contentValues.put("news_status", news.getNews_status());
        return db.insert("fav_news", null, contentValues) != -1;
    }

//    ---------- select all data from any table ---------

    public Cursor getAllDate(String table_name) {
        return getWritableDatabase().rawQuery("select * from "+table_name+" ORDER BY ID DESC", null);
    }

    // -- get saved news by news_id --
    public Cursor getOneNews(String table_name, String news_id) {
        return getWritableDatabase().rawQuery("select * from "+table_name+" where ID="+news_id, null);
    }

    //    --------- deleting data from any table ------
    public void delete(String table_name) {
        getWritableDatabase().execSQL("delete from "+table_name);
    }
    // ----------- get the last record from any table --------
    public Cursor getLastRecord (String table_name, String column_name){
        return getWritableDatabase().rawQuery("SELECT * FROM "+table_name+" ORDER BY "+column_name+" DESC LIMIT 1",null);
    }
    //    ------ delete item by id -------
    public void deleteById(String tbale_name, String deleted_item_id) {
        getWritableDatabase().execSQL("delete from "+tbale_name+" where ID= "+deleted_item_id);
    }
    // --- update news status --
    public boolean updateData (String id, String table_name, String check_value, String colmun_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(colmun_name, Integer.parseInt(check_value));
        db.update(table_name, contentValues, "ID = ?", new String[]{id});
        return true;
    }
    // --- update news body detail --
    public boolean updateNewsDetail (String id, String table_name, String check_value, String colmun_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(colmun_name, check_value);
        db.update(table_name, contentValues, "ID = ?", new String[]{id});
        return true;
    }
    //    ----------- get all fav news --------
    public Cursor getAllFavNews() {
        return getWritableDatabase().rawQuery("select * from fav_news where news_status='1' ORDER BY ID DESC", null);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS fav_news");
        onCreate(db);
    }
}

