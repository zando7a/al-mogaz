package almogaz.orangestudio.com.Model;

/**
 * Created by orange on 03/04/17.
 */

public class OneNews {
    int news_id,news_viewsNum;
    String news_title, news_details, news_date, news_image, news_source;

    public OneNews(int news_id, int news_viewsNum, String news_title, String news_details, String news_date, String news_image, String news_source) {
        this.news_id = news_id;
        this.news_viewsNum = news_viewsNum;
        this.news_title = news_title;
        this.news_details = news_details;
        this.news_date = news_date;
        this.news_image = news_image;
        this.news_source = news_source;
    }

    public int getNews_id() {
        return news_id;
    }

    public void setNews_id(int news_id) {
        this.news_id = news_id;
    }

    public int getNews_viewsNum() {
        return news_viewsNum;
    }

    public void setNews_viewsNum(int news_viewsNum) {
        this.news_viewsNum = news_viewsNum;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_details() {
        return news_details;
    }

    public void setNews_details(String news_details) {
        this.news_details = news_details;
    }

    public String getNews_date() {
        return news_date;
    }

    public void setNews_date(String news_date) {
        this.news_date = news_date;
    }

    public String getNews_image() {
        return news_image;
    }

    public void setNews_image(String news_image) {
        this.news_image = news_image;
    }

    public String getNews_source() {
        return news_source;
    }

    public void setNews_source(String news_source) {
        this.news_source = news_source;
    }
}
