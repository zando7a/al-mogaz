package almogaz.orangestudio.com.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.Indexable;
import com.tapadoo.alerter.Alerter;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Controller.NewsAdapter;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.DataBaseHelper;
import almogaz.orangestudio.com.Helper.Utils;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.Model.Search;
import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Activities.SearchResult;

public class LatestNews extends Fragment{
    public static String TAG = LatestNews.class.getSimpleName();
    // -- recycler view parameters
    ArrayList<News> newses = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    RecyclerView rv_news;
    NewsAdapter newsAdapter;
    // -- loader
    GeometricProgressView loader;
    // -- show one news item type
    public static int SHOW_TYPE = 2;
    // -- page number for pagination
    int page_num = 0;
    private boolean loading = true;
    // -- define Constants class
    Constants constants;
    // -- refresh part
    SwipeRefreshLayout refreshLayout;
    // -- alert linear layout appear when api return null
    LinearLayout btn_alert_missed_data;
    Button btn_reload;
    // -- search category id
    String search_catID;
    // -- Google indexing --
    News news_indexing;
    // -- database helper class for SQlite data
    DataBaseHelper db_helper ;
    public LatestNews() {
        // Required empty public constructor
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static LatestNews newInstance(String text) {

        LatestNews f = new LatestNews();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Utils.onActivityCreateSetTheme(getActivity());
        // -- inflate fragment layout
        View v = inflater.inflate(R.layout.fragment_latest_news, container, false);
        // -- database helper
        db_helper = new DataBaseHelper(getContext());
        // --- constants class
        constants = new Constants(getActivity());
        // -- google interstitial ads
        MobileAds.initialize(getContext(),getResources().getString(R.string.ADMOB_APP_ID));
        FirebaseApp.initializeApp(getContext());
        /*
            refresh recycler view ... reload more news
         */
        refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // -- check for internet availability before refresh
                if (constants.isNetworkAvailable()){
                    // Refresh items
                    newses.clear();
                    page_num = 0;
                    newsAdapter = new NewsAdapter(newses, getContext(), SHOW_TYPE,getFragmentManager());
                    rv_news.setAdapter(newsAdapter);
                    makeJsonArrayRequest();
                }
                else {
                    noConnection();
                }
            }
        });
        /*
            Main recycler view manager ..
         */
        rv_news = (RecyclerView) v.findViewById(R.id.rv_news);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_news.setLayoutManager(mLayoutManager);
        // --- loader initialization
        loader = (GeometricProgressView) v.findViewById(R.id.progressView);
        loader.setNumberOfAngles(16);
        loader.setColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        // --- check if internet connection is available
        newsAdapter = new NewsAdapter(newses, getContext(), SHOW_TYPE,getFragmentManager());
        rv_news.setAdapter(newsAdapter);
        constants = new Constants(getActivity());

        if (constants.isNetworkAvailable()){
            makeJsonArrayRequest();
        }
        else {
            /*
                show no connection alert dialog
                & reload data from sqlite database
             */
            noConnection();
            // --
            Cursor cursor = db_helper.getAllDate("fav_news");
            if (cursor.getCount()!=0){
                cursor.moveToFirst();
                for (int i =0; i<cursor.getCount(); i++){
                    newses.add(new News(""+cursor.getInt(0),cursor.getString(1),cursor.getString(2),
                            cursor.getString(3),cursor.getString(4),cursor.getString(7),cursor.getString(5),
                            cursor.getString(6),"","",cursor.getInt(8)));
                    cursor.moveToNext();
                }
                newsAdapter.notifyDataSetChanged();
                rv_news.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            }
        }
        // --- detect reach to the end of recycler view to load more data
        rv_news.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0){
                    if (loading) {
                        if (constants.isNetworkAvailable()){
                            loading = false;
                            page_num ++;
                            makeJsonArrayRequest();
                        }
//                        else {
//                            noConnection();
//                        }
                    }
                }
            }
        });
        // -- floating button go to the top of recycler view
        final FloatingActionButton fb_Up = (FloatingActionButton)v.findViewById(R.id.fb_getTop);
        fb_Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rv_news.smoothScrollToPosition(0);
            }
        });
        //--- show and hide floating button based on recycler view position
        rv_news.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (mLayoutManager.findFirstVisibleItemPosition()==0)
                    fb_Up.hide();
//                else if (dy > 0)
//                    lp.setMargins(0,0,6,64);

                else
                    fb_Up.show();
            }
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//
//            if (newState == RecyclerView.SCROLL_STATE_IDLE)
//                    fb_Up.show();
//                super.onScrollStateChanged(recyclerView, newState);
//            }
        });
        // --- show in case of connection request timeout and json retrun null value --

        btn_alert_missed_data = (LinearLayout) v.findViewById(R.id.btn_alert_missed_data);
        btn_reload = (Button) v.findViewById(R.id.btn_reload);
        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_alert_missed_data.setVisibility(View.GONE);
                loader.setVisibility(View.VISIBLE);
                makeJsonArrayRequest();
            }
        });

//        ----------- SMS -----------
        ImageButton ib_send_message_toVodafone = (ImageButton) v.findViewById(R.id.ib_send_message_toVodafone);
        ib_send_message_toVodafone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constants.openSmsApps(getContext(),"9999","9090");
            }
        });
//        =====================
        ImageButton ib_send_message_toEtisalat = (ImageButton) v.findViewById(R.id.ib_send_message_toEtisalat);
        ib_send_message_toEtisalat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constants.openSmsApps(getContext(),"1666","9090");
            }
        });
//        ======================
        ImageButton ib_send_message_toOrange = (ImageButton) v.findViewById(R.id.ib_send_message_toOrange);
        ib_send_message_toOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constants.openSmsApps(getContext(),"8187","9090");
            }
        });

        // -- change show item type
        final Button btn_showType = (Button) v.findViewById(R.id.btn_changeViewitem);
        btn_showType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SHOW_TYPE==1){
                    SHOW_TYPE=2;
                    btn_showType.setCompoundDrawablesWithIntrinsicBounds(null,null,ContextCompat.getDrawable(getContext(),R.drawable.ic_show_style_white1),null);
                }
                else {
                    SHOW_TYPE=1;
                    btn_showType.setCompoundDrawablesWithIntrinsicBounds(null,null,ContextCompat.getDrawable(getContext(),R.drawable.ic_show_style_white2),null);
                }
                newsAdapter = new NewsAdapter(newses, getContext(),SHOW_TYPE,getFragmentManager());
                rv_news.setAdapter(newsAdapter);
            }
        });

        final Button btn_search = (Button) v.findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_method();
            }
        });
        return v;
    }

    private void makeJsonArrayRequest() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.getCache().clear();
        JsonArrayRequest req = new JsonArrayRequest("http://almogaz.com/api/api_get_all_news?page=" + page_num,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("", response.toString());
                        News saved_news;
                        int news_status = 0;
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject news = (JSONObject) response
                                        .get(i);

                                if (constants.checkFav(Integer.parseInt(news.getString("news_id")))){
                                    news_status=1;
                                }
                                else {
                                    news_status =0;
                                }

                                if (i==1&&page_num==1){
                                    newses.add(new News("videos_section","","","","","","","","","",0));
                                }
                                if (news.isNull("views_num")) {
                                    saved_news = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "لا يوجد مشاهدات", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                    news_indexing = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "لا يوجد مشاهدات", news.getString("type"), news.getString("page_url"),"","","",0);
                                    indexTask(news_indexing);
                                } else {
                                    saved_news =new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "222", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                    news_indexing = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "222", news.getString("type"), news.getString("page_url"),"","","",0);
                                    indexTask(news_indexing);
                                }

                                if (((i+1)%6)==0){
                                    newses.add(new News("adsPartItem_fb","","","","","","","","","",0));
                                }
                                else if (((i+1)%3)==0){
                                    newses.add(new News("adsPartItem_google","","","","","","","","","",0));
                                }
                                db_helper.allNewsData(saved_news);
                            }
                            loading = true;
                            rv_news.setVisibility(View.VISIBLE);
                            newsAdapter.notifyItemRangeInserted(newsAdapter.getItemCount(), newses.size()-1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                        loader.setVisibility(View.GONE);
                        refreshLayout.setRefreshing(false);
                        btn_alert_missed_data.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
                loader.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                btn_alert_missed_data.setVisibility(View.VISIBLE);
            }

        }) {

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(50000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        if (constants.isNetworkAvailable()) {
            req.setShouldCache(false);
        } else {
            Alerter.create(getActivity())
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        req.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(req);
    }


    //    ----------- indexing ----
    public void indexTask(final News news) {
        Indexable recipeToIndex = new Indexable.Builder()
                .setName(news.getNews_title())
                .setUrl(news.getPage_url())
                .setImage(news.getNews_thumb())
                .setDescription(news.getNews_source())
                .build();

        Task<Void> task = FirebaseAppIndex.getInstance().update(recipeToIndex);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "App Indexing API: Successfully added " + news.getNews_title() + " to " +
                        "index");
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.e(TAG, "App Indexing API: Failed to add " + news.getNews_title() + " to index. " +
                        "" + exception.getMessage());
            }
        });
    }

//    ------------

    private void search_method(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(),R.style.AlerDialogTheme);
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View dialogView = inflater.inflate(R.layout.search_alert_dailog, null);

        dialogView.setBackgroundColor(Color.parseColor(Utils.background));

        dialogBuilder.setView(dialogView);
        final AlertDialog dialogTest = dialogBuilder.create();
        dialogTest.setCancelable(true);
        dialogTest.show();

//               ----------------- category spinner implementation -----------------------------
        final ArrayList<String> categories_spinner = new ArrayList<>();
        final ArrayList<String> categories_spinner_id = new ArrayList<>();
        categories_spinner.add("كل الأقسام");
        categories_spinner_id.add("");
        Spinner spinner = (Spinner) dialogView.findViewById(R.id.spinner);
        for (int i=1; i<MainCategories.categories.size(); i++){
            categories_spinner.add(MainCategories.categories.get(i-1).getCategory_name());
            categories_spinner_id.add(MainCategories.categories.get(i-1).getCategory_id());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,categories_spinner);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(getActivity().getPackageName(), ""+categories_spinner.get(position).toString()
                        +categories_spinner_id.get(position).toString()
                );
                search_catID = categories_spinner_id.get(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//              ------ date of start and end search result implementation -----------
        final TextView tv_alert_title = (TextView) dialogView.findViewById(R.id.tv_alertTitle);
        final DatePicker datePicker = (DatePicker)  dialogView.findViewById(R.id.date_picker_news);
        final RelativeLayout search_container = (RelativeLayout) dialogView.findViewById(R.id.search_container);

        final TextView tv_start_search_date, tv_end_search_date ;
        tv_start_search_date = (TextView) dialogView.findViewById(R.id.tv_start_search_date);
        tv_end_search_date = (TextView) dialogView.findViewById(R.id.tv_end_search_date);

        tv_start_search_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.setVisibility(View.VISIBLE);
                search_container.setVisibility(View.GONE);
                tv_alert_title.setText("تاريخ بداية الخبر");
            }
        });
        tv_end_search_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.setVisibility(View.VISIBLE);
                search_container.setVisibility(View.GONE);
                tv_alert_title.setText("تاريخ نهاية الخبر");
            }
        });
//               --------------- when date selected -----------------

        datePicker.init(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (tv_alert_title.getText().equals("تاريخ بداية الخبر")){
                    datePicker.setVisibility(View.GONE);
                    search_container.setVisibility(View.VISIBLE);
                    tv_alert_title.setText("خيارات البحث");
                    tv_start_search_date.setText(datePicker.getYear()+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getDayOfMonth());
                }
                else if (tv_alert_title.getText().equals("تاريخ نهاية الخبر")){
                    datePicker.setVisibility(View.GONE);
                    search_container.setVisibility(View.VISIBLE);
                    tv_alert_title.setText("خيارات البحث");
                    tv_end_search_date.setText(datePicker.getYear()+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getDayOfMonth());
                }
            }
        });
//                --------------------- search key word field ------------------
        final EditText editText = (EditText) dialogView.findViewById(R.id.editText);

        //              ----------cancel and done buttons actions --------------------


        Button btn_ok, btn_cancel;
        btn_ok = (Button) dialogView.findViewById(R.id.bt_done);
        btn_cancel = (Button) dialogView.findViewById(R.id.bt_cancel);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Search search = new Search(""+tv_start_search_date.getText().toString(),""+tv_end_search_date.getText().toString(),""+search_catID,""+editText.getText().toString());
                Intent intent = new Intent(getActivity(), SearchResult.class);
                intent.putExtra("search_key",search);
                startActivity(intent);
                dialogTest.dismiss();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogTest.dismiss();
            }
        });
    }

    // --- show failed connection alert dailog
    private void noConnection (){
        Alerter.create(getActivity())
                .setTitle(R.string.no_internet_connection)
                .setText(R.string.no_internet_connection_detail)
                .setBackgroundColor(R.color.colorAccent)
                .show();
    }
}
