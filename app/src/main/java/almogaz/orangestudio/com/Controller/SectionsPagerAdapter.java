package almogaz.orangestudio.com.Controller;

/**
 * Created by orange on 22/02/17.
 */
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Fragments.LatestNews;
import almogaz.orangestudio.com.View.Fragments.MainCategories;
import almogaz.orangestudio.com.View.Fragments.MyNews;
import almogaz.orangestudio.com.View.Fragments.Videos;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    private final List<Fragment> mFragmentList = new ArrayList<>();
    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0: return LatestNews.newInstance("FirstFragment");
            case 1: return Videos.newInstance("SecondFargment");
            case 2: return MyNews.newInstance("SecondFragment");
            case 3: return MainCategories.newInstance("forthFragment");
            default: return LatestNews.newInstance("ThirdFragment, Default");
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getResources().getString(R.string.last_news);
            case 1:
                return mContext.getResources().getString(R.string.MainCategories);
            case 2:
                return mContext.getResources().getString(R.string.MainCategories);
        }
        return null;
    }

    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }
}
