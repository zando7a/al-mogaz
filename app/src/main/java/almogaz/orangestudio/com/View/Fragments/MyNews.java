package almogaz.orangestudio.com.View.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import almogaz.orangestudio.com.Controller.NewsAdapter;
import almogaz.orangestudio.com.Helper.DataBaseHelper;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.R;

public class MyNews extends Fragment {

    DataBaseHelper db_helper;
    ArrayList<News> news = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    RecyclerView rv_news;
    NewsAdapter newsAdapter;
    TextView tv_emptyFavNewsAlert ;
    public MyNews() {
        // Required empty public constructor
    }
    public static MyNews newInstance(String text) {
        MyNews fragment = new MyNews();
        Bundle b = new Bundle();
        b.putString("msg", text);

        fragment.setArguments(b);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        rv_news = (RecyclerView) view.findViewById(R.id.rv_news);
        tv_emptyFavNewsAlert = (TextView) view.findViewById(R.id.tv_emptyFavNewsAlert);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        news.clear();
        db_helper = new DataBaseHelper(getContext());
        Cursor cursor = db_helper.getAllFavNews();
        if (cursor.getCount()!=0){
            cursor.moveToFirst();
            tv_emptyFavNewsAlert.setVisibility(View.GONE);
            for (int i =0; i<cursor.getCount(); i++){
                news.add(new News(""+cursor.getInt(0),cursor.getString(1),cursor.getString(2),
                        cursor.getString(3),cursor.getString(4),cursor.getString(7),cursor.getString(5),
                        cursor.getString(6),"","",cursor.getInt(8)));
                cursor.moveToNext();
            }
        }
        else {
            tv_emptyFavNewsAlert.setVisibility(View.VISIBLE);
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_news.setLayoutManager(mLayoutManager);
        newsAdapter = new NewsAdapter(news, getContext(), LatestNews.SHOW_TYPE);
        rv_news.setAdapter(newsAdapter);
    }
}