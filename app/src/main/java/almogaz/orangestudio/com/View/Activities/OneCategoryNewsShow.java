package almogaz.orangestudio.com.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.MobileAds;
import com.tapadoo.alerter.Alerter;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Controller.NewsAdapter;
import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.Font;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Fragments.LatestNews;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OneCategoryNewsShow extends Activity {

    ArrayList<News> newses = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    GeometricProgressView loader;
    RecyclerView rv_news;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page_num = 0;
    NewsAdapter newsAdapter;
    Constants constants;
    public static String CATEGORY_ID ;
    int mode;
    SwipeRefreshLayout refreshLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

//      ------------ font -------------
        new Font().fontStyle();
// -----------------------------------
        mode= SettingsAdapter.mode;
        if(mode==0){
            this.setTheme(R.style.AppTheme);
        }
        else if(mode==1){
            setTheme(R.style.AppTheme1);
        }
        else {
            setTheme(R.style.AppTheme2);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_category_news_show);

        MobileAds.initialize(OneCategoryNewsShow.this,getResources().getString(R.string.ADMOB_APP_ID));

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                newses.clear();
                page_num =0;
                newsAdapter = new NewsAdapter(newses, OneCategoryNewsShow.this,LatestNews.SHOW_TYPE);
                rv_news.setAdapter(newsAdapter);
                makeJsonArrayRequest();
            }
        });

        rv_news = (RecyclerView) findViewById(R.id.rv_news);
        mLayoutManager = new LinearLayoutManager(OneCategoryNewsShow.this);
        rv_news.setLayoutManager(mLayoutManager);
        rv_news.setHasFixedSize(true);
        loader = (GeometricProgressView) findViewById(R.id.progressView);
        loader.setNumberOfAngles(16);
        loader.setColor(ContextCompat.getColor(OneCategoryNewsShow.this,R.color.colorPrimary));
        newsAdapter = new NewsAdapter(newses, OneCategoryNewsShow.this, LatestNews.SHOW_TYPE);
        rv_news.setAdapter(newsAdapter);
        constants = new Constants(OneCategoryNewsShow.this);

        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        makeJsonArrayRequest();
        if (constants.isNetworkAvailable()){
            makeJsonArrayRequest();
        }
        else {
            Alerter.create(OneCategoryNewsShow.this)
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .show();
            makeJsonArrayRequest();
        }

        rv_news.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            page_num++;
                            makeJsonArrayRequest();

                            if (constants.isNetworkAvailable()){
                                loading = false;
                                page_num ++;
                                makeJsonArrayRequest();
                            }
                            else {
                                Alerter.create(OneCategoryNewsShow.this)
                                        .setTitle(R.string.no_internet_connection)
                                        .setText(R.string.no_internet_connection_detail)
                                        .setBackgroundColor(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_signal_wifi_off)
                                        .show();
                                makeJsonArrayRequest();
                            }
                        }
                    }
                }
            }
        });
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void makeJsonArrayRequest() {
        if (constants.isNetworkAvailable()) {
            RequestQueue queue = Volley.newRequestQueue(OneCategoryNewsShow.this);
            queue.getCache().clear();
        } else {
            Alerter.create(OneCategoryNewsShow.this)
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        JsonArrayRequest req = new JsonArrayRequest("http://almogaz.com/api/get_one_category?tid="+CATEGORY_ID+"&page="+page_num,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("", response.toString());
                        try {
                            News saved_news;
                            int news_status = 0;
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject news = (JSONObject) response
                                        .get(i);

                                if (constants.checkFav(Integer.parseInt(news.getString("news_id")))){
                                    news_status=1;
                                }
                                else {
                                    news_status =0;
                                }

                                if (news.isNull("views_num")) {
                                    saved_news = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "لا يوجد مشاهدات", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                } else {
                                    saved_news = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "2222", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                }

                                if (((i+1)%6)==0){
                                    newses.add(new News("adsPartItem_fb","","","","","","","","","",0));
                                }
                                else if (((i+1)%3)==0){
                                    newses.add(new News("adsPartItem_google","","","","","","","","","",0));
                                }
                            }
                            loading = true;
                            rv_news.setVisibility(View.VISIBLE);
                            newsAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(OneCategoryNewsShow.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        loader.setVisibility(View.GONE);
                        refreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
//                Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                loader.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
            }
        }){

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(50000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        if (constants.isNetworkAvailable()) {
            req.setShouldCache(false);
        } else {
            Alerter.create(OneCategoryNewsShow.this)
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        req.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(req);
    }

}
