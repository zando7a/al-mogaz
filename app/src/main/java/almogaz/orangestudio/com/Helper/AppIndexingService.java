package almogaz.orangestudio.com.Helper;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.builders.Indexables;

import java.util.ArrayList;
import java.util.List;

import almogaz.orangestudio.com.Model.News;

/**
 * Created by Same7-Orange on 12/18/2017.
 */

public class AppIndexingService extends IntentService {
    public AppIndexingService() {
        super("AppIndexingService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        ArrayList<Indexable> indexableNotes = new ArrayList<>();

        for (News news : getAllRecipes()) {
            if (news != null) {
                Indexable noteToIndex = Indexables.noteDigitalDocumentBuilder()
                        .setName(news.getNews_title())
                        .setImage(news.getNews_thumb())
                        .setText(news.getNews_source())
                        .setUrl(news.getPage_url())
                        .build();

                indexableNotes.add(noteToIndex);
            }
        }

        if (indexableNotes.size() > 0) {
            Indexable[] notesArr = new Indexable[indexableNotes.size()];
            notesArr = indexableNotes.toArray(notesArr);

            // batch insert indexable notes into index
            FirebaseAppIndex.getInstance().update(notesArr);
        }
    }

    // [START_EXCLUDE]
    private List<News> getAllRecipes() {
        ArrayList news = new ArrayList();
        // Code access all recipes with their notes from the database here.
        return news;
    }
    // [END_EXCLUDE]
}
