package almogaz.orangestudio.com.Model;

import java.io.Serializable;

/**
 * Created by orange on 14/03/17.
 */

public class News implements Serializable{
    String news_id, news_title, news_thumb, news_date, views_num, type, page_url;
//    ----------
    String news_detail, news_image, news_source;
//    ---
    int news_status;

    public News(String news_id, String news_title, String news_thumb, String news_date, String views_num, String type, String page_url, String news_detail, String news_image, String news_source, int news_status) {
        this.news_id = news_id;
        this.news_title = news_title;
        this.news_thumb = news_thumb;
        this.news_date = news_date;
        this.views_num = views_num;
        this.type = type;
        this.page_url = page_url;
        this.news_detail = news_detail;
        this.news_image = news_image;
        this.news_source = news_source;
        this.news_status = news_status;
    }


    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_thumb() {
        return news_thumb;
    }

    public void setNews_thumb(String news_thumb) {
        this.news_thumb = news_thumb;
    }

    public String getNews_date() {
        return news_date;
    }

    public void setNews_date(String news_date) {
        this.news_date = news_date;
    }

    public String getViews_num() {
        return views_num;
    }

    public void setViews_num(String views_num) {
        this.views_num = views_num;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public String getNews_detail() {
        return news_detail;
    }

    public void setNews_detail(String news_detail) {
        this.news_detail = news_detail;
    }

    public String getNews_image() {
        return news_image;
    }

    public void setNews_image(String news_image) {
        this.news_image = news_image;
    }

    public String getNews_source() {
        return news_source;
    }

    public void setNews_source(String news_source) {
        this.news_source = news_source;
    }

    public int getNews_status() {
        return news_status;
    }

    public void setNews_status(int news_status) {
        this.news_status = news_status;
    }
}
