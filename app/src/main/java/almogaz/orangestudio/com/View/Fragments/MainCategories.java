package almogaz.orangestudio.com.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.tapadoo.alerter.Alerter;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Controller.CategoryAdapter;
import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.Utils;
import almogaz.orangestudio.com.Model.Category;
import almogaz.orangestudio.com.R;

public class MainCategories extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private LinearLayoutManager mLayoutManager;
    GeometricProgressView loader;
    RecyclerView rv_categories;
    public static ArrayList<Category> categories = new ArrayList<>();
    CategoryAdapter categoryAdapter;
    Constants constants;
    int mode;

    public MainCategories() {
        // Required empty public constructor
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainCategories newInstance(String text) {

        MainCategories f = new MainCategories();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mode = SettingsAdapter.mode;
        final Context contextThemeWrapper;
        //--------------*---------------------
        Utils.onActivityCreateSetTheme(getActivity());
//        if(mode==0){
//            contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
//        }
//        else if(mode==1){
//            contextThemeWrapper= new ContextThemeWrapper(getActivity(), R.style.AppTheme1);
//        }
//        else {
//            contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme2);
//        }

        // clone the inflater using the ContextThemeWrapper
//        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
        View v = inflater.inflate(R.layout.fragment_news_sites, container, false);
        constants = new Constants(getActivity());
        rv_categories = (RecyclerView) v.findViewById(R.id.rv_categories);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_categories.setLayoutManager(mLayoutManager);
        rv_categories.setHasFixedSize(true);
        loader = (GeometricProgressView) v.findViewById(R.id.progressView);
        loader.setNumberOfAngles(16);
        loader.setColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        categoryAdapter = new CategoryAdapter(categories, getActivity());
        rv_categories.setAdapter(categoryAdapter);
        makeJsonArrayRequest();

        //        ----------- SMS ----
        ImageButton ib_send_message_toVodafone = (ImageButton) v.findViewById(R.id.ib_send_message_toVodafone);
        ib_send_message_toVodafone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constants.openSmsApps(getContext(), "9999", "9090");
            }
        });
//        =====================
        ImageButton ib_send_message_toEtisalat = (ImageButton) v.findViewById(R.id.ib_send_message_toEtisalat);
        ib_send_message_toEtisalat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constants.openSmsApps(getContext(), "1666", "9090");
            }
        });
//        ======================
        ImageButton ib_send_message_toOrange = (ImageButton) v.findViewById(R.id.ib_send_message_toOrange);
        ib_send_message_toOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constants.openSmsApps(getContext(), "8187", "9090");
            }
        });

        return v;
    }

    private void makeJsonArrayRequest() {
        if (constants.isNetworkAvailable()) {
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            queue.getCache().clear();
        } else {
            Alerter.create(getActivity())
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        JsonArrayRequest req = new JsonArrayRequest("http://almogaz.com/api/api_get_main_categories",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("", response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject category = (JSONObject) response
                                        .get(i);
                                categories.add(new Category(category.getString("category_id"), category.getString("category_name")));
                            }
                            rv_categories.setVisibility(View.VISIBLE);
                            categoryAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                        loader.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
                loader.setVisibility(View.GONE);
            }
        }) {

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(50000
                , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        if (constants.isNetworkAvailable()) {
            req.setShouldCache(false);
        } else {
            Alerter.create(getActivity())
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        req.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(req);
    }
}

