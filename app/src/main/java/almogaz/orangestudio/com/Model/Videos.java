package almogaz.orangestudio.com.Model;

/**
 * Created by sameh on 06/07/17.
 */

public class Videos {
    String video_type, video_url, video_id;

    public Videos(String video_type, String video_url, String video_id) {
        this.video_type = video_type;
        this.video_url = video_url;
        this.video_id = video_id;
    }

    public String getVideo_type() {
        return video_type;
    }

    public void setVideo_type(String video_type) {
        this.video_type = video_type;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }
}
