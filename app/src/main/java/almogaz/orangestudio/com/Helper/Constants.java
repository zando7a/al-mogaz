package almogaz.orangestudio.com.Helper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by orange on 05/04/17.
 */

public class Constants {

    Context context ;
    public static String API_KEY = "IPS9SBc7fJMpK5hfaMb9rg";
    public static String TEST_DEVICE_ID = "3ACC0F281E5D9A399A0255E975C4271D";
    DataBaseHelper db_helper ;
    Cursor cursor;
    public Constants(Context context) {
        this.context = context;
        db_helper = new DataBaseHelper(context);
        cursor = db_helper.getAllFavNews();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public String diffOfTime (String newsDate){
        String final_result ="" ;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy - HH:mm");
            Date news_date = sdf.parse(newsDate);
            String currentDate = sdf.format(new Date());
            Date current_date = sdf.parse(currentDate);
            long diff_time = current_date.getTime()-news_date.getTime();
            int time ;
            if (diff_time<=60000){
                final_result = "الآن";
            }
            else if (60000<diff_time && 3600000>diff_time){
                time = (int) (diff_time/60000) ;
                final_result = ""+time+" دقيقة ";
            }
            else if (3600000<diff_time && 86400000>diff_time){
                time = (int) (diff_time/3600000) ;
                if (time==1){
                    final_result = " ساعة ";
                }
                else if (time==2){
                    final_result = " ساعتان ";
                }
                else if (2<time && 11>time){
                    final_result = " "+time+" ساعات ";
                }
                else {
                    final_result = " "+time+" ساعة ";
                }
            }
            else if (86400000<diff_time && (86400000*7)>diff_time){
                time = (int) (diff_time/86400000) ;
                if (time==1){
                    final_result = " يوم ";
                }
                else if (time==2){
                    final_result = " يومان ";
                }
                else if (2<time && 7>time){
                    final_result = " "+time+" ايام ";
                }
                else {
                    final_result = " "+time+" اسبوع ";
                }
            }
            else {
                final_result = newsDate ;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return final_result ;
    }

    public void openSmsApps(Context context, String number, String message){
        Uri uri = Uri.parse("smsto:"+number);
        Intent sms_intent = new Intent(Intent.ACTION_SENDTO, uri);
        sms_intent.putExtra("sms_body", message);
        context.startActivity(sms_intent);
    }

    public void shareNews(String news_title ,String news_url){
        String message = news_title+" "+news_url;
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);

        context.startActivity(Intent.createChooser(share, "مشاركة الخبر"));
    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        private String TAG = "DownloadImage";
        String image_name ;
        private Bitmap downloadImageBitmap(String sUrl) {
            image_name = sUrl.replace("/","_");
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d(TAG, "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        protected void onPostExecute(Bitmap result) {
            saveImage(context, result, image_name);
        }
    }

    public void saveImage(Context context, Bitmap b, String imageName) {
        FileOutputStream foStream;
        try {
            foStream = context.openFileOutput(imageName, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
            foStream.close();
        } catch (Exception e) {
            Log.d("saveImage", "Exception 2, Something went wrong!");
            e.printStackTrace();
        }
    }

    public ArrayList<String> getAllHtmlImages(String htmlBody){
        ArrayList<String> Images = new ArrayList<>();
        String pattern = "(http|https)://.*.(?:jpg|png|jpeg)";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(htmlBody);
        while (m.find()){
            Images.add(m.group());
        }
        for (int i=0; i<Images.size(); i++){
            new DownloadImage().execute(Images.get(i));
            Log.e("Images",Images.get(i).replace("/","_"));
        }
        return Images;
    }

    public boolean checkFav(int news_id){
        boolean checked = false ;
        cursor.moveToFirst();
        for (int i=0; i<cursor.getCount(); i++){
            if (news_id==cursor.getInt(0)){
                checked = true;
                break;
            }
            cursor.moveToNext();
        }
        return checked;
    }

}
