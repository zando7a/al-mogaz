package almogaz.orangestudio.com.Helper;

import almogaz.orangestudio.com.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by ahalima on 10/07/17.
 */

public class Font {
    public void fontStyle(){
    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/cairo_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );}
}
