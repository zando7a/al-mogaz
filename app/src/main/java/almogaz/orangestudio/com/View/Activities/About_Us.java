package almogaz.orangestudio.com.View.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.Utils;
import almogaz.orangestudio.com.R;

public class About_Us extends AppCompatActivity {

    TextView txt_header,txt_content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.onActivityCreateSetTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about__us);

        txt_header=(TextView)findViewById(R.id.toolbar_title);
        txt_content=(TextView)findViewById(R.id.txt_content);
//        txt_content.setTextSize(14+new FontSize().fontSize(SettingsAdapter.size));

        if(SettingsAdapter.page==1)
        {
            txt_header.setText(getString(R.string.about_us));
            jsonObject("http://almogaz.com/api/about-us","privacy");
        }
        else
        {
            txt_header.setText(getString(R.string.privacy_policy));
            jsonObject("http://almogaz.com/api/privacy","privacy");
        }
    }

    public void back(View view) {
        finish();
    }

    public void jsonObject(String apiRequest, final String name){

        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET,apiRequest,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject news = (JSONObject) response.get(i);
                                txt_content.setText(news.getString(name));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("api-key", Constants.API_KEY);
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }
}
