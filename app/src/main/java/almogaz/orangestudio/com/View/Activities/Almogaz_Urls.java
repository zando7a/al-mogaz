package almogaz.orangestudio.com.View.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.R;

public class Almogaz_Urls extends Activity {
    WebView webView;
    int mode;
    public static String ACCOUNT_URL ;
    private ProgressDialog progressBar2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mode= SettingsAdapter.mode;
        if(mode==0){
            this.setTheme(R.style.AppTheme);
        }
        else if(mode==1){
            setTheme(R.style.AppTheme1);
        }
        else {
            setTheme(R.style.AppTheme2);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_almogaz__urls);

        progressBar2 = new ProgressDialog(this);
        progressBar2.setMessage("");

        webView = (WebView) findViewById(R.id.wv_AlmogazUrls);
        webView.clearCache(true);
        webView.clearHistory();
        webView.setWebViewClient(new AppWebViewClients(progressBar2));
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.loadUrl(ACCOUNT_URL);
        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public class AppWebViewClients extends WebViewClient {
        private ProgressDialog progressBar;
        public AppWebViewClients(ProgressDialog progressBar) {
            this.progressBar=progressBar;
            progressBar.show();
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            view.setVisibility(View.INVISIBLE);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            view.setVisibility(View.VISIBLE);
            progressBar.dismiss();
        }
    }
}
