package almogaz.orangestudio.com.View.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.DataBaseHelper;
import almogaz.orangestudio.com.Helper.Font;
import almogaz.orangestudio.com.Helper.FontSize;
import almogaz.orangestudio.com.Helper.Utils;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OneNewsShow extends YouTubeBaseActivity{

    public static final String TAG = OneNewsShow.class.getSimpleName();
    News newses ;
    TextView tv_newsTitle, tv_seenNumbers, tv_newsSource ;
    WebView wv_newsDetails;
    ImageView iv_newsimage ;
    Constants constants ;
    FloatingActionButton fb_share ;
    CoordinatorLayout coordinatorLayout;


    AdView mAdView;
    int size;
    Activity context;
    public static int mode2=0;
    public static int size2=0;
    LinearLayout linearLayout;

    final String mimeType = "text/html";
    final String encoding = "UTF-8";
    private com.facebook.ads.AdView adView;

    DataBaseHelper db_helper ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//      ------------ font -------------
        new Font().fontStyle();

        Utils.onActivityCreateSetTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_news_show);

        context=this;
        constants = new Constants(this);

        db_helper = new DataBaseHelper(this);

        Intent intent = getIntent();
        newses = (News) intent.getSerializableExtra("object_key");

        Log.e("ID_news",""+newses.getNews_id());
        tv_newsTitle = (TextView) findViewById(R.id.tv_newsTitle);
        tv_newsSource = (TextView) findViewById(R.id.tv_newsSource);
        tv_seenNumbers = (TextView) findViewById(R.id.tv_seen_time);
        iv_newsimage = (ImageView) findViewById(R.id.iv_newsimage);
        wv_newsDetails = (WebView) findViewById(R.id.wv_newsDetails);

        wv_newsDetails.getSettings().setJavaScriptEnabled(true);
        wv_newsDetails.getSettings().setLoadWithOverviewMode(true);
        wv_newsDetails.getSettings().setSupportZoom(true);
        wv_newsDetails.getSettings().setBuiltInZoomControls(true);
        wv_newsDetails.getSettings().setDisplayZoomControls(false);
        size = SettingsAdapter.size;   //size from sittings
        tv_newsSource.setTextSize(14+new FontSize().fontSize(size));
        tv_seenNumbers.setTextSize(12+new FontSize().fontSize(size));
        tv_newsTitle.setTextSize(19+new FontSize().fontSize(size));
        tv_newsTitle.setTypeface(null, Typeface.BOLD);

        linearLayout=(LinearLayout)findViewById(R.id.linearlayout);
        linearLayout.setBackgroundColor(Color.parseColor(Utils.background));

        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.coordinatorlayout);
        coordinatorLayout.setBackgroundColor(Color.parseColor(Utils.background));

        Cursor cursor = db_helper.getOneNews("fav_news",newses.getNews_id());
        cursor.moveToFirst();
        if (cursor.getCount()!=0&&!cursor.getString(6).equals("")){

            wv_newsDetails.loadDataWithBaseURL("",
                    "<html dir=\"rtl\"><body style=\""+"font-size:"+FontSize.fontsize+";"+"background-color:"+Utils.background+" ; color:"+Utils.fontcolor+" ;\">"
                            +cursor.getString(6)
                            + "<style>iframe{display: block; max-width: 100% !important;}img{display: block;height:auto;max-width: 100% !important;}</style>" + "</body></html>",
                    mimeType,
                    encoding, "");
            Picasso.with(OneNewsShow.this)
                    .load(newses.getNews_thumb())
                    .placeholder(R.drawable.bg_white_black)
                    .into(iv_newsimage);
        }
        else {
            if (constants.isNetworkAvailable()){
                makeJsonObjectRequest();
            }
            else {
                Toast.makeText(OneNewsShow.this, "خدمة الانترنت غير متوافرة", Toast.LENGTH_SHORT).show();
            }
        }


        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        //back button
        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.i==1)
                    startActivity(new Intent(getApplicationContext(),Home.class));
                else
                    finish();
                Utils.i=0;
            }
        });

        //font_size button
        ImageButton font_size = (ImageButton) findViewById(R.id.bt_font_size);
        font_size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater)getBaseContext()
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.font_size_dailog, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,true);
                popupWindow.showAsDropDown(v, 50, -30);
                TextView textView=(TextView)popupView.findViewById(R.id.tv_alertTitle);
                textView.setVisibility(View.GONE);

                RadioGroup radiogroup=(RadioGroup)popupView.findViewById(R.id.radios1);
                final RadioButton small_font1=(RadioButton) popupView.findViewById(R.id.small_font1);
                final RadioButton medium_font1=(RadioButton) popupView.findViewById(R.id.medium_font1);
                final RadioButton big_font1=(RadioButton) popupView.findViewById(R.id.big_font1);

                popupView.setBackgroundColor(Color.parseColor(Utils.background));
                small_font1.setTextColor(Color.parseColor(Utils.fontcolor));
                medium_font1.setTextColor(Color.parseColor(Utils.fontcolor));
                big_font1.setTextColor(Color.parseColor(Utils.fontcolor));

                if(size2==0)
                    small_font1.setChecked(true);
                else if(size2==1)
                    medium_font1.setChecked(true);
                else
                    big_font1.setChecked(true);


                radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                        if(checkedId == R.id.small_font1)
                            size2=0;

                        else if(checkedId == R.id.medium_font1)
                            size2=1;

                        else
                            size2=2;
                        Utils.i=1;
                        SettingsAdapter.size=size2;
                        popupWindow.dismiss();
                        startActivity(getIntent());
                        finish();
                    }
                });
            }
        });

        //style button
        final ImageButton mode = (ImageButton) findViewById(R.id.mode);
        mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater)getBaseContext()
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.reading_style_dailog, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,true);
                popupWindow.showAsDropDown(v, 50, -30);
                TextView textView=(TextView)popupView.findViewById(R.id.tv_alertTitle);
                textView.setVisibility(View.GONE);

                RadioGroup radiogroup=(RadioGroup)popupView.findViewById(R.id.radios);
                RadioButton small_font=(RadioButton) popupView.findViewById(R.id.small_font);
                RadioButton medium_font=(RadioButton) popupView.findViewById(R.id.medium_font);
                RadioButton big_font=(RadioButton) popupView.findViewById(R.id.big_font);

                popupView.setBackgroundColor(Color.parseColor(Utils.background));
                small_font.setTextColor(Color.parseColor(Utils.fontcolor));
                medium_font.setTextColor(Color.parseColor(Utils.fontcolor));
                big_font.setTextColor(Color.parseColor(Utils.fontcolor));

                if(SettingsAdapter.mode == 0)
                    small_font.setChecked(true);
                else if(SettingsAdapter.mode == 1)
                    medium_font.setChecked(true);
                else
                    big_font.setChecked(true);


                radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                        //---------*---------------
                        if(checkedId == R.id.small_font)
                            mode2=0;
                        else if(checkedId == R.id.medium_font)
                            mode2=1;
                        else
                            mode2=2;
                        Utils.i=1;
                        SettingsAdapter.mode=mode2;
                        Utils.sTheme=mode2;
                        popupWindow.dismiss();
                        startActivity(getIntent());
                        finish();
                    }
                });
            }
        });

        fb_share = (FloatingActionButton) findViewById(R.id.fb_share);
        fb_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.shareNews(newses.getNews_title(),newses.getPage_url());
            }
        });


        tv_newsSource.setText("منذ "+constants.diffOfTime(newses.getNews_date())+" | "+newses.getType());
        tv_newsTitle.setText(newses.getNews_title().replaceAll("&quot;",""));


        RelativeLayout adViewContainer = (RelativeLayout) findViewById(R.id.adViewContainer);

        adView = new com.facebook.ads.AdView(this, "140879569954442_140880426621023", AdSize.BANNER_320_50);
        adViewContainer.addView(adView);
        adView.loadAd();

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

//    --------- return news details ---------------

    private void makeJsonObjectRequest() {

        final ProgressDialog dialog = new ProgressDialog(OneNewsShow.this);
        dialog.setMessage("loading");
        dialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://almogaz.com/api/get_one_new/"+newses.getNews_id(), null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    String saved_html = response.getString("body_html");
                    ArrayList<String> html_images = new ArrayList<>();
                    html_images = constants.getAllHtmlImages(response.getString("body_html"));
                    if (html_images.size()!=0){
                        for (int i = 0; i<html_images.size(); i++){
                            saved_html = saved_html.replace(html_images.get(i),"file:///data/user/0/almogaz.orangestudio.com/files/"+html_images.get(i).replaceAll("/","_"));
                            Log.e("Images_html ", html_images.get(i));
                        }
                    }
                    db_helper.updateNewsDetail(newses.getNews_id(),"fav_news",saved_html,"news_detail");

                    wv_newsDetails.loadDataWithBaseURL("",
                            "<html dir=\"rtl\"><body style=\""+"font-size:"+FontSize.fontsize+";"+"background-color:"+Utils.background+" ; color:"+Utils.fontcolor+" ;\">"
                            +response.getString("body_html")
                            + "<style>iframe{display: block; max-width: 100% !important;}img{display: block;height:auto;max-width: 100% !important;}</style>" + "</body></html>",
                            mimeType,
                            encoding, "");
                    Log.e("body_html",response.getString("body_html"));
                    Glide.with(OneNewsShow.this)
                            .load(newses.getNews_thumb())
                            .into(iv_newsimage);
                    tv_seenNumbers.setText(response.getString("resource"));
//                    boolean check = db_helper.updateNewsDetail(newses.getNews_id(),"fav_news",response.getString("body_html"),"news_detail");
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    Picasso.with(OneNewsShow.this)
                            .load("https://pbs.twimg.com/profile_images/1575554561/tp_400x400.jpg")
                            .placeholder(R.drawable.bg_white_black)
                            .into(iv_newsimage);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // hide the progress dialog
                dialog.dismiss();
            }
        }){

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(50000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onBackPressed() {
        if(Utils.i==1)
            startActivity(new Intent(getApplicationContext(),Home.class));
        else
            finish();
        Utils.i=0;
    }
}
