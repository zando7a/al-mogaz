package almogaz.orangestudio.com.View.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.MobileAds;
import com.tapadoo.alerter.Alerter;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Controller.NewsAdapter;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.DataBaseHelper;
import almogaz.orangestudio.com.Helper.Font;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.Model.Search;
import almogaz.orangestudio.com.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchResult extends AppCompatActivity {

    Search search;
    Constants constants ;
    ArrayList<News> newses = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    GeometricProgressView loader;
    public static int SHOW_TYPE =2;
    private boolean loading = true;
    RecyclerView rv_news;
    NewsAdapter newsAdapter ;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page_num = 0;
    DataBaseHelper db_helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

//      ------------ font -------------
        new Font().fontStyle();
// ------------------------------------

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        MobileAds.initialize(this,getResources().getString(R.string.ADMOB_APP_ID));
        db_helper = new DataBaseHelper(this);
        constants = new Constants(this);
        Intent intent = getIntent();
        search = (Search) intent.getExtras().get("search_key");

        newsAdapter = new NewsAdapter(newses,this,SHOW_TYPE);

        rv_news = (RecyclerView) findViewById(R.id.rv_searchResult);

        mLayoutManager = new LinearLayoutManager(this);
        rv_news.setLayoutManager(mLayoutManager);
        rv_news.setHasFixedSize(true);
        loader = (GeometricProgressView)findViewById(R.id.progressView);
        loader.setNumberOfAngles(16);
        loader.setColor(ContextCompat.getColor(this,R.color.colorPrimary));

        rv_news.setAdapter(newsAdapter);

        makeJsonArrayRequest();

        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv_news.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            page_num++;
                            makeJsonArrayRequest();

                            if (constants.isNetworkAvailable()){
                                loading = false;
                                page_num ++;
                                makeJsonArrayRequest();
                            }
                            else {
                                Alerter.create(SearchResult.this)
                                        .setTitle(R.string.no_internet_connection)
                                        .setText(R.string.no_internet_connection_detail)
                                        .setBackgroundColor(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_signal_wifi_off)
                                        .show();
                                makeJsonArrayRequest();
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
//    ----------------------
    private void makeJsonArrayRequest() {
        if (constants.isNetworkAvailable()) {
            RequestQueue queue = Volley.newRequestQueue(SearchResult.this);
            queue.getCache().clear();
        } else {
            Alerter.create(SearchResult.this)
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        String final_category_id, final_startDate, final_endDate;

        if (search.getSearch_catID().equals("")||search.getSearch_catID().isEmpty()){
            final_category_id = "All";
        }
        else {
            final_category_id = search.getSearch_catID();
        }
        if (search.getSearch_startDate().isEmpty()||search.getSearch_startDate().equals(null)){
            final_startDate = "";
        }
        else {
            final_startDate = search.getSearch_startDate();
        }
        if (search.getSearch_endDate().isEmpty()||search.getSearch_endDate().equals(null)){
            final_endDate = "";
        }
        else {
            final_endDate = search.getSearch_endDate();
        }
        JsonArrayRequest req = new JsonArrayRequest("http://almogaz.com/api/search?combine="+search.getSearch_keyWord()+
                "&field_news_section_tid="+final_category_id+
                "&created="+final_startDate+
                "&created_1="+final_endDate+
                "&page="+page_num,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("", response.toString());
                        News saved_news;
                        int news_status = 0;
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject news = (JSONObject) response
                                        .get(i);

                                if (constants.checkFav(Integer.parseInt(news.getString("nid")))){
                                    news_status=1;
                                }
                                else {
                                    news_status =0;
                                }

                                if (news.isNull("views_num")) {
                                    saved_news = new News(news.getString("nid"), news.getString("title"), news.getString("news_thumb"), news.getString("date"),
                                            "لا يوجد مشاهدات", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                } else {
                                    saved_news = new News(news.getString("nid"), news.getString("title"), news.getString("news_thumb"), news.getString("date"),
                                            "222", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                }
                                if (((i+1)%6)==0){
                                    newses.add(new News("adsPartItem_fb","","","","","","","","","",0));
                                }
                                else if (((i+1)%3)==0){
                                    newses.add(new News("adsPartItem_google","","","","","","","","","",0));
                                }
                                db_helper.allNewsData(saved_news);
                            }
                            loading = true;
                            rv_news.setVisibility(View.VISIBLE);
                            newsAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(SearchResult.this,
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                        loader.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
                loader.setVisibility(View.GONE);
            }
        }){

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(50000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        if (constants.isNetworkAvailable()) {
            req.setShouldCache(false);
        } else {
            Alerter.create(this)
                    .setTitle(R.string.no_internet_connection)
                    .setText(R.string.no_internet_connection_detail)
                    .setBackgroundColor(R.color.colorAccent)
                    .setIcon(R.drawable.ic_signal_wifi_off)
                    .show();
        }
        req.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(req);
    }
}
