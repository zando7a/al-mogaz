package almogaz.orangestudio.com.View.Activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdRequest;
import com.tapadoo.alerter.Alerter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import almogaz.orangestudio.com.Controller.AppController;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.DataBaseHelper;
import almogaz.orangestudio.com.Helper.Typewriter;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.R;

public class SplashScreen extends Activity {

    Constants constants;
    DataBaseHelper db_helper;
    public static ArrayList<News> newses = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        constants = new Constants(this);
        Typewriter writer = (Typewriter)findViewById(R.id.typewriter);

        //Add a character every 150ms
        writer.setCharacterDelay(100);
        writer.animateText("www.Almogaz.com\n"+"كل الأخبار .. باختصار");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, Home.class));
                finish();
            }
        }, 4000);

        makeJsonArrayRequest();
    }

    private void makeJsonArrayRequest() {
        JsonArrayRequest req = new JsonArrayRequest("http://almogaz.com/api/get_one_category?tid=2&page=0",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("", response.toString());
                        try {
                            News saved_news;
                            int news_status = 0;
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject news = (JSONObject) response
                                        .get(i);

                                if (constants.checkFav(Integer.parseInt(news.getString("news_id")))){
                                    news_status=1;
                                }
                                else {
                                    news_status =0;
                                }

                                if (news.isNull("views_num")) {
                                    saved_news = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "لا يوجد مشاهدات", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                } else {
                                    saved_news = new News(news.getString("news_id"), news.getString("news_title"), news.getString("news_thumb"), news.getString("news_date"),
                                            "2222", news.getString("type"), news.getString("page_url"),"","","",news_status);
                                    newses.add(saved_news);
                                }
                            }

//                            startActivity(new Intent(SplashScreen.this, Home.class));
//                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(SplashScreen.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
            }
        }){

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(50000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(req);
    }
}
