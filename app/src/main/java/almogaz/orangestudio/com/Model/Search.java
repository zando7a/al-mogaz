package almogaz.orangestudio.com.Model;

import java.io.Serializable;

/**
 * Created by sameh on 04/06/17.
 */

public class Search implements Serializable{
    String search_startDate, search_endDate, search_catID, search_keyWord;

    public Search(String search_startDate, String search_endDate, String search_catID, String search_keyWord) {
        this.search_startDate = search_startDate;
        this.search_endDate = search_endDate;
        this.search_catID = search_catID;
        this.search_keyWord = search_keyWord;
    }

    public String getSearch_startDate() {
        return search_startDate;
    }

    public void setSearch_startDate(String search_startDate) {
        this.search_startDate = search_startDate;
    }

    public String getSearch_endDate() {
        return search_endDate;
    }

    public void setSearch_endDate(String search_endDate) {
        this.search_endDate = search_endDate;
    }

    public String getSearch_catID() {
        return search_catID;
    }

    public void setSearch_catID(String search_catID) {
        this.search_catID = search_catID;
    }

    public String getSearch_keyWord() {
        return search_keyWord;
    }

    public void setSearch_keyWord(String search_keyWord) {
        this.search_keyWord = search_keyWord;
    }
}
