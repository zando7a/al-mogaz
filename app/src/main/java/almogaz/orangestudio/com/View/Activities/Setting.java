package almogaz.orangestudio.com.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.Helper.Font;
import almogaz.orangestudio.com.Helper.Utils;
import almogaz.orangestudio.com.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Setting extends Activity {
    RecyclerView rv_settings ;
    LinearLayoutManager linearLayoutManager ;
    boolean [] setting_type = {true, false,false,false,false,
            true, false,false,false,false,false,
            true, false,false,false,false,false};
    int mode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//      ------------ font -------------
        new Font().fontStyle();
// -----------------------------------
        Utils.onActivityCreateSetTheme(this);
//        mode= SettingsAdapter.mode;
//        if(mode==0){
//            setTheme(R.style.AppTheme);
//        }
//        else if(mode==1){
//            setTheme(R.style.AppTheme1);
//        }
//        else {
//            setTheme(R.style.AppTheme2);
//        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        rv_settings = (RecyclerView) findViewById(R.id.rv_setting);
        linearLayoutManager = new LinearLayoutManager(this);
        rv_settings.setLayoutManager(linearLayoutManager);
        rv_settings.setHasFixedSize(true);

        SettingsAdapter settingsAdapter = new SettingsAdapter(this,setting_type);
        rv_settings.setAdapter(settingsAdapter);

        //-------**********************------------------
        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.i==1)
                    startActivity(new Intent(getApplicationContext(),Home.class));
                else
                    finish();
                Utils.i=0;
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public void onBackPressed() {
        if(Utils.i==1)
            startActivity(new Intent(getApplicationContext(),Home.class));
        else
            finish();
        Utils.i=0;
    }
}
