package almogaz.orangestudio.com.Controller;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import almogaz.orangestudio.com.R;

/**
 * Created by sameh on 04/07/17.
 */

public class ImageSliderPagerAdapter extends PagerAdapter {
    ArrayList<String> images_url ;
    Context context ;

    public ImageSliderPagerAdapter(ArrayList<String> images_url, Context context) {
        this.images_url = images_url;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.one_news_image_for_slider, collection, false);
//        ---------- news Image -------------
        ImageView iv_newsimage = (ImageView) layout.findViewById(R.id.iv_one_news_image);
        Picasso.with(context)
                .load(images_url.get(position))
                .into(iv_newsimage);

        collection.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        return images_url.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }
}
