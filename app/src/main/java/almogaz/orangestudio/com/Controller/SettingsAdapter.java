package almogaz.orangestudio.com.Controller;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.Utils;
import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Activities.About_Us;
import almogaz.orangestudio.com.View.Activities.Almogaz_Urls;
import almogaz.orangestudio.com.View.Activities.OneNewsShow;

/**
 * Created by orange on 11/04/17.
 */

public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ViewHolder>{
    public static int mode=0;
    public static int size=0;
    public static int page=0;
    Activity context ;
    boolean [] setting_type ;
    TextView tv_title;
    Constants constants;
    Fragment frg;
    public SettingsAdapter(Activity context, boolean[] setting_type) {
        this.context = context;
        Utils.onActivityCreateSetTheme(context);
        this.setting_type = setting_type;
        constants = new Constants(context);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tv_categoryName;
        public ViewHolder(TextView tv_categoryName) {
            super(tv_categoryName);
            this.tv_categoryName = tv_categoryName;
        }
    }

    @Override
    public SettingsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.one_setting_item, parent, false);
        SettingsAdapter.ViewHolder vh = new SettingsAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final SettingsAdapter.ViewHolder holder, final int position) {
        if (setting_type[position]){
            holder.tv_categoryName.setPadding(8,8,12,8);

            holder.tv_categoryName.setTextColor(Color.parseColor(Utils.fontcolor));

            holder.tv_categoryName.setTextSize(20);
            if(mode==0)
                holder.tv_categoryName.setBackgroundColor(ContextCompat.getColor(context,R.color.gray_color));
            else
                holder.tv_categoryName.setBackgroundColor(ContextCompat.getColor(context,R.color.backgroundcard1));
        }
        else {
            holder.tv_categoryName.setPadding(12,12,48,12);
            holder.tv_categoryName.setTextColor(Color.parseColor(Utils.fontcolor));
        }
        holder.tv_categoryName.setText(context.getResources().getStringArray(R.array.setting_names)[position]);
        TypedArray imgs = context.getResources().obtainTypedArray(R.array.setting_icon);
        holder.tv_categoryName.setCompoundDrawablesWithIntrinsicBounds(0,0,imgs.getResourceId(position,-1),0);
        holder.tv_categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogBuilder ;
                LayoutInflater inflater ;
                final View dialogView;
                final RadioGroup radiogroup;
                final AlertDialog dialogTest ;
                Button btn_done, btn_cancel;
                switch (position){
                    case 0:
                        break;
                    case 1:
                        dialogBuilder = new AlertDialog.Builder(context,R.style.AlerDialogTheme);
                        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                        dialogView = inflater.inflate(R.layout.font_size_dailog, null);
                        radiogroup=(RadioGroup)dialogView.findViewById(R.id.radios1);
                        final RadioButton small_font1=(RadioButton) dialogView.findViewById(R.id.small_font1);
                        final RadioButton medium_font1=(RadioButton) dialogView.findViewById(R.id.medium_font1);
                        final RadioButton big_font1=(RadioButton) dialogView.findViewById(R.id.big_font1);
                        tv_title = (TextView) dialogView.findViewById(R.id.tv_alertTitle);

                        dialogView.setBackgroundColor(Color.parseColor(Utils.background));
                        small_font1.setTextColor(Color.parseColor(Utils.fontcolor));
                        medium_font1.setTextColor(Color.parseColor(Utils.fontcolor));
                        big_font1.setTextColor(Color.parseColor(Utils.fontcolor));
                        tv_title.setTextColor(Color.parseColor(Utils.fontcolor));

                        if(size==0)
                            small_font1.setChecked(true);
                        else if(size==1)
                            medium_font1.setChecked(true);
                        else
                            big_font1.setChecked(true);

                        dialogBuilder.setView(dialogView);
                        dialogTest = dialogBuilder.create();
                        dialogTest.show();

                        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                                        if(checkedId == R.id.small_font1)
                                            size=0;
                                        else if(checkedId == R.id.medium_font1)
                                            size=1;
                                        else
                                            size=2;
                                        OneNewsShow.size2=size;
                                        context.recreate();

                                        Utils.i=1;
                                        dialogTest.dismiss();
                                    }
                                });

                        break;

                        case 2:
                            dialogBuilder = new AlertDialog.Builder(context,R.style.AlerDialogTheme);
                            inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                            dialogView = inflater.inflate(R.layout.reading_style_dailog, null);

                            dialogView.setBackgroundColor(Color.parseColor(Utils.background));
                            radiogroup=(RadioGroup)dialogView.findViewById(R.id.radios);
                            RadioButton small_font=(RadioButton) dialogView.findViewById(R.id.small_font);
                            RadioButton medium_font=(RadioButton) dialogView.findViewById(R.id.medium_font);
                            RadioButton big_font=(RadioButton) dialogView.findViewById(R.id.big_font);
                            tv_title = (TextView) dialogView.findViewById(R.id.tv_alertTitle);

                            small_font.setTextColor(Color.parseColor(Utils.fontcolor));
                            medium_font.setTextColor(Color.parseColor(Utils.fontcolor));
                            big_font.setTextColor(Color.parseColor(Utils.fontcolor));
                            tv_title.setTextColor(Color.parseColor(Utils.fontcolor));

                            if(mode == 0)
                                small_font.setChecked(true);

                            else if(mode == 1)
                                medium_font.setChecked(true);

                            else
                                big_font.setChecked(true);

                            dialogBuilder.setView(dialogView);
                            dialogTest = dialogBuilder.create();
                            dialogTest.show();

                            radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                                //---------*---------------
                                if(checkedId == R.id.small_font) {
                                    mode=0;
                                    Utils.changeToTheme(context, mode);
                                }
                                else if(checkedId == R.id.medium_font){
                                    mode=1;
                                    Utils.changeToTheme(context, mode);
                                }
                                else {
                                    mode=2;
                                    Utils.changeToTheme(context, mode);

                                }
                                Utils.i=1;
                                dialogTest.dismiss();
                            }
                            });

                        break;
                    case 3:
                        dialogBuilder = new AlertDialog.Builder(context,R.style.AlerDialogTheme);
                        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                        dialogView = inflater.inflate(R.layout.notification_alert_dailog, null);

                        dialogView.setBackgroundColor(Color.parseColor(Utils.background));
                        dialogBuilder.setView(dialogView);
                        dialogTest = dialogBuilder.create();
                        dialogTest.show();
                        final Switch switch_on_off_Notifications = (Switch) dialogView.findViewById(R.id.switch_on_off_Notifications);
                        final Switch switch_on_off_Sound = (Switch) dialogView.findViewById(R.id.switch_on_off_Sound);
                        tv_title = (TextView) dialogView.findViewById(R.id.tv_alertTitle);
                        btn_done = (Button) dialogView.findViewById(R.id.bt_done);
                        btn_cancel = (Button) dialogView.findViewById(R.id.bt_cancel);

                        switch_on_off_Notifications.setTextColor(Color.parseColor(Utils.fontcolor));
                        switch_on_off_Sound.setTextColor(Color.parseColor(Utils.fontcolor));
                        tv_title.setTextColor(Color.parseColor(Utils.fontcolor));
                        btn_done.setTextColor(Color.parseColor(Utils.fontcolor));
                        btn_cancel.setTextColor(Color.parseColor(Utils.fontcolor));

                        btn_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogTest.dismiss();
                            }
                        });
                        btn_done.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogTest.dismiss();
                            }
                        });
                        break;
                    case 4:
                        dialogBuilder = new AlertDialog.Builder(context,R.style.AlerDialogTheme);
                        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                        dialogView = inflater.inflate(R.layout.short_message_services, null);
                        dialogView.setBackgroundColor(Color.parseColor(Utils.background));

                        dialogBuilder.setView(dialogView);
                        dialogTest = dialogBuilder.create();
                        dialogTest.show();

                        //        ----------- SMS ----
                        ImageButton ib_send_message_toVodafone = (ImageButton) dialogView.findViewById(R.id.ib_send_message_toVodafone);
                        ib_send_message_toVodafone.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                constants.openSmsApps(context,"9999","9090");
                            }
                        });
//        =====================
                        ImageButton ib_send_message_toEtisalat = (ImageButton) dialogView.findViewById(R.id.ib_send_message_toEtisalat);
                        ib_send_message_toEtisalat.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                constants.openSmsApps(context,"1666","9090");
                            }
                        });
//        ======================
                        ImageButton ib_send_message_toOrange = (ImageButton) dialogView.findViewById(R.id.ib_send_message_toOrange);
                        ib_send_message_toOrange.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                constants.openSmsApps(context,"8187","9090");
                            }
                        });
                        break;
                    case 5:
                        break;
                    case 6:
                        Almogaz_Urls.ACCOUNT_URL="http://almogaz.com";
                        context.startActivity(new Intent(context,Almogaz_Urls.class));
                        break;
                    case 7:
                        Almogaz_Urls.ACCOUNT_URL="https://m.facebook.com/AlmogazNews";
                        context.startActivity(new Intent(context,Almogaz_Urls.class));
                        break;
                    case 8:
                        Almogaz_Urls.ACCOUNT_URL="https://mobile.twitter.com/Almogaz";
                        context.startActivity(new Intent(context,Almogaz_Urls.class));
                        break;
                    case 9:
                        Almogaz_Urls.ACCOUNT_URL="https://plus.google.com/+Almogaz";
                        context.startActivity(new Intent(context,Almogaz_Urls.class));
                        break;
                    case 10:
                        String urlApp = "تطبيق الموجز .. كل الأخبار بإختصار \n"+"https://play.google.com/store/apps/details?id=almogaz.orangstudio.com";
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("text/plain");
                        share.putExtra(Intent.EXTRA_TEXT, urlApp);
                        context.startActivity(Intent.createChooser(share,"مشاركة التطبيق"));
                        break;
                    case 11:
                        break;
                    case 12:
                        page=1;
                        context.startActivity(new Intent(context,About_Us.class));
                        break;
                    case 13:
                        Toast.makeText(context, holder.tv_categoryName.getText() , Toast.LENGTH_SHORT).show();
                        break;
                    case 14:
                        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                        break;
                    case 15:
                        dialogBuilder = new AlertDialog.Builder(context,R.style.AlerDialogTheme);
                        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                        dialogView = inflater.inflate(R.layout.custom_alert_dailog, null);

                        dialogView.setBackgroundColor(Color.parseColor(Utils.background));
                        dialogBuilder.setView(dialogView);
                        dialogTest = dialogBuilder.create();
                        dialogTest.setCancelable(false);
                        dialogTest.show();
                        TextView tv_content;
                        tv_title = (TextView) dialogView.findViewById(R.id.tv_alertTitle);
                        tv_content = (TextView) dialogView.findViewById(R.id.tv_alertContent);
                        tv_title.setText(holder.tv_categoryName.getText());
                        try {
                            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                            tv_content.setText("Version Name: "+pInfo.versionName+"\n"+"Version Number: "+pInfo.versionCode);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        Button bt_done = (Button) dialogView.findViewById(R.id.bt_done);

                        tv_title.setTextColor(Color.parseColor(Utils.fontcolor));
                        tv_content.setTextColor(Color.parseColor(Utils.fontcolor));
                        bt_done.setTextColor(Color.parseColor(Utils.fontcolor));

                        bt_done.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogTest.dismiss();
                            }
                        });
                        break;

                    case 16:
                        page=2;
                        context.startActivity(new Intent(context,About_Us.class));
                        break;
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return setting_type.length;
    }
}
