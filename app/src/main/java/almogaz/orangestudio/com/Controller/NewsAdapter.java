package almogaz.orangestudio.com.Controller;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdsManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.like.LikeButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.DataBaseHelper;
import almogaz.orangestudio.com.Helper.FontSize;
import almogaz.orangestudio.com.Helper.Native_Ads_Advanced;
import almogaz.orangestudio.com.Model.News;
import almogaz.orangestudio.com.Model.Videos;
import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Activities.Caricature_Image;
import almogaz.orangestudio.com.View.Activities.OneNewsShow;
import almogaz.orangestudio.com.View.Activities.SplashScreen;

/**
 * Created by orange on 22/02/17.
 */

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    // ---- main constructor components
    static Context mContext ;
    ArrayList<News> newses ;
    Constants constants ;
    int show_type ;
    /* all ads components
            for both Google ads
                and FB ads
         */
    // -------- interstitial ads
    InterstitialAd mInterstitialAd;
    // ------- FB native ads
    private NativeAd mAd ;
    private NativeAdsManager adsManager;
    // --- SQLITE database
    DataBaseHelper db_helper;
    // -- fragment
    private FragmentManager fragmentManager;
    FragmentTransaction transaction ;
    android.support.v4.app.Fragment fragment = new almogaz.orangestudio.com.View.Fragments.Videos();
    //------- main constructor --
    public NewsAdapter(ArrayList<News> newses, Context mContext, int mShow_type) {
        // ---- main constructor components
        this.newses = newses;
        this.mContext = mContext;
        this.show_type = mShow_type;
        constants = new Constants(mContext);
        /* all ads components
            for both Google ads
                and FB ads
         */
        // -------- interstitial ads
        mInterstitialAd = new InterstitialAd(mContext);
        mInterstitialAd.setAdUnitId("ca-app-pub-7358343163125843/1145518452");
        // ------- FB native ads
        mAd = new NativeAd(mContext,"140879569954442_140880223287710");
        mAd.loadAd();
        // -- DataBaseHelper
        db_helper = new DataBaseHelper(mContext);
        // -- fragment
    }

    public NewsAdapter(ArrayList<News> newses, Context mContext, int mShow_type,FragmentManager fragment) {
        // ---- main constructor components
        this.newses = newses;
        this.mContext = mContext;
        this.show_type = mShow_type;
        constants = new Constants(mContext);
        /* all ads components
            for both Google ads
                and FB ads
         */
        // -------- interstitial ads
        mInterstitialAd = new InterstitialAd(mContext);
        mInterstitialAd.setAdUnitId("ca-app-pub-7358343163125843/1145518452");
        // ------- FB native ads
        mAd = new NativeAd(mContext,"140879569954442_140880223287710");
        mAd.loadAd();
        // -- DataBaseHelper
        db_helper = new DataBaseHelper(mContext);
        // -- fragment
        this.fragmentManager = fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v =null ;
        View c;
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        int size = SettingsAdapter.size;
        int mode = SettingsAdapter.mode;
        if (viewType==1){
            // --- inflate main news item
            if (show_type == 1) {
                v = (LinearLayout) LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.one_news_item_2, parent, false);
            }
            else if (show_type == 2){
                v = (LinearLayout) LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.one_news_item, parent, false);
            }
            else if (show_type == 3){
                v = (LinearLayout) LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.one_news_item_3, parent, false);
            }
            TextView tv_newsSource, tv_seenNum, tv_addFav, tv_newsTime, tv_newsTitle;
            ImageButton btn_share;
            LikeButton  btn_read_later ;
            ImageView iv_newsImg = (ImageView) v.findViewById(R.id.iv_newsImage);
            ImageView iv_videoNews = (ImageView) v.findViewById(R.id.iv_videoNews);
            CardView cardView = (CardView) v.findViewById(R.id.card_view);
            tv_newsSource = (TextView) v.findViewById(R.id.tv_newsSource);
            tv_seenNum = (TextView) v.findViewById(R.id.tv_seenNum);
            tv_addFav = (TextView) v.findViewById(R.id.tv_addFav);
            tv_newsTime = (TextView) v.findViewById(R.id.tv_newsTime);
            tv_newsTitle = (TextView) v.findViewById(R.id.tv_newsTitle);
            btn_share = (ImageButton) v.findViewById(R.id.btn_share);
            btn_read_later = (LikeButton) v.findViewById(R.id.btn_read_later);
            viewHolder = new ViewHolder(v, tv_newsSource, tv_seenNum, tv_addFav, tv_newsTime, tv_newsTitle, iv_newsImg, iv_videoNews,btn_share,btn_read_later);

            if (mode != 0){
                cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.backgroundcard));
            }

            tv_newsSource.setTextSize(12 + new FontSize().fontSize(size));
            tv_seenNum.setTextSize(10 + new FontSize().fontSize(size));
            tv_newsTime.setTextSize(10 + new FontSize().fontSize(size));
            tv_newsTitle.setTextSize(14 + new FontSize().fontSize(size));
        }
//        --------------------------------------
        else if (viewType==2){
            // --- inflate Google adv item --
            c = inflater.inflate(R.layout.ad_mob_item, parent, false);
            viewHolder = new ViewHolderAdMob(c);
        }
//        ----------------------------------------
        else if (viewType==3) {
            // --- inflate FB adv item

            mAd = new NativeAd(mContext,"140879569954442_140880223287710");
            mAd.loadAd();
            v = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.ad_unit, parent, false);

            LinearLayout conatainer = (LinearLayout) v.findViewById(R.id.native_ad_unit);
            ImageView nativeAdIcon = (ImageView) v.findViewById(R.id.native_ad_icon);
            TextView nativeAdTitle = (TextView) v.findViewById(R.id.native_ad_title);
            MediaView nativeAdMedia = (MediaView) v.findViewById(R.id.native_ad_media);
            TextView nativeAdSocialContext = (TextView) v.findViewById(R.id.native_ad_social_context);
            TextView nativeAdBody = (TextView) v.findViewById(R.id.native_ad_body);
            Button nativeAdCallToAction = (Button) v.findViewById(R.id.native_ad_call_to_action);

            viewHolder = new AdHolder(conatainer, nativeAdMedia, nativeAdIcon, nativeAdTitle, nativeAdBody, nativeAdSocialContext, nativeAdCallToAction);
        }
        else if (viewType==4){
            v = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.videos_item, parent, false);

            RecyclerView rv_news_section = (RecyclerView) v.findViewById(R.id.rv_videos_section);

            TextView tv_moreVideo = (TextView) v.findViewById(R.id.tv_moreVideo);
            viewHolder = new ViewHolderVideoSection(v,rv_news_section,tv_moreVideo);
        }
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch(holder.getItemViewType()){
            case 1:{
                ViewHolder viewHolder = (ViewHolder) holder;
                // -- set news source text view
                viewHolder.tv_newsSource.setText(newses.get(position).getType());
                // -- set news seen number text view
                viewHolder.tv_seenNum.setText(newses.get(position).getViews_num());
                // -- set news title text view
                viewHolder.tv_newsTitle.setText(newses.get(position).getNews_title().replaceAll("[-+.^:,#;&$@!%*quot]",""));
                // -- set news date text view
                viewHolder.tv_newsTime.setText("منذ "+constants.diffOfTime(newses.get(position).getNews_date()));
                // -- set news thumb image view
                try {
                    Picasso.with(mContext)
                            .load(newses.get(position).getNews_thumb())
                            .placeholder(R.drawable.bg_white_black)
                            .into(viewHolder.iv_newsImg);
                }
                catch (Exception e){
                    // --- if news thumb invalid path set with ALMOGAZ logo
                    Picasso.with(mContext)
                            .load("https://pbs.twimg.com/profile_images/1575554561/tp_400x400.jpg")
                            .into(viewHolder.iv_newsImg);
                    Log.e("Error","error loading image");
                }
                // --- set video icon for news that have video or from توك شو category
                if (newses.get(position).getType().equals("توك شو")||newses.get(position).getNews_title().contains("فيديو")){
                    viewHolder.iv_videoNews.setVisibility(View.VISIBLE);
                }
                else {
                    viewHolder.iv_videoNews.setVisibility(View.GONE);
                }
                // -- one item click listener
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!newses.get(position).getType().equals("كاريكاتير")){
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                                Intent intent = new Intent(mContext, OneNewsShow.class) ;
                                intent.putExtra("object_key",newses.get(position));
                                mContext.startActivity(intent);
                            }
                            else {
                                Intent intent = new Intent(mContext, OneNewsShow.class) ;
                                intent.putExtra("object_key",newses.get(position));
                                mContext.startActivity(intent);
                            }
                        }
                        else {
                            Intent intent = new Intent(mContext, Caricature_Image.class) ;
                            intent.putExtra("object_key_image",newses.get(position).getNews_thumb());
                            intent.putExtra("object_key_title",newses.get(position).getNews_title().replaceAll("[-+.^:,#;&$@!%*quot]",""));
                            intent.putExtra("object_key_url",newses.get(position).getPage_url());
                            Bundle bundle= ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext,v.findViewById(R.id.iv_newsImage),"myImage").toBundle();
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                                mContext.startActivity(intent,bundle);
                            }
                            else {

                                mContext.startActivity(intent,bundle);
                            }
                        }
                        requestNewInterstitial();

                    }
                });
                viewHolder.btn_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        constants.shareNews(newses.get(position).getNews_title().replaceAll("&quot;",""),newses.get(position).getPage_url());
                    }
                });
                // --- read later click listener
                if (newses.get(position).getNews_status()==1){
                    viewHolder.btn_read_later.setLiked(true);
                }
                else {
                    viewHolder.btn_read_later.setLiked(false);
                }
                viewHolder.btn_read_later.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (newses.get(position).getNews_status()==1){
                            newses.get(position).setNews_status(0);
                            db_helper.updateData(newses.get(position).getNews_id(),"fav_news","0","news_status");
                            notifyDataSetChanged();
                            Toast.makeText(mContext, "تم حذف الخبر من قائمة أخباري", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            if (constants.isNetworkAvailable()){
                                newses.get(position).setNews_status(1);
                                db_helper.updateData(newses.get(position).getNews_id(),"fav_news","1","news_status");
                                notifyDataSetChanged();
                                makeJsonObjectRequest(newses.get(position).getNews_id());
                            }
                            else {
                                Toast.makeText(mContext, " يجب توافر خدمة الانترنت لحفظ الخبر فى قائمة أخباري", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                break;
            }
            //  -- google ads
            case 2:{
                break;
            }
            // --- FB ads
            case 3:{
                final AdHolder adHolder = (AdHolder) holder;
                mAd.setAdListener(new AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Toast.makeText(mContext, "error: "+adError.getErrorMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        adHolder.container.setVisibility(View.VISIBLE);
                        // Set the Text.
                        adHolder.mAdTitle.setText(mAd.getAdTitle());
                        adHolder.mAdSocialContext.setText(mAd.getAdSocialContext());
                        adHolder.mAdBody.setText(mAd.getAdBody());
                        adHolder.mAdCallToAction.setText(mAd.getAdCallToAction());

                        // Download and display the ad icon.
                        NativeAd.Image adIcon = mAd.getAdIcon();
                        NativeAd.downloadAndDisplayImage(adIcon, adHolder.mAdIcon);

                        // Download and display the cover image.
                        adHolder.mAdMedia.setNativeAd(mAd);

                        List<View> clickableViews = new ArrayList<>();
                        clickableViews.add(adHolder.mAdTitle);
                        clickableViews.add(adHolder.mAdCallToAction);
                        mAd.registerViewForInteraction(adHolder.container,clickableViews);
                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {

                    }
                });
                break;
            }
            case 4:{
                ViewHolderVideoSection videos_holder = (ViewHolderVideoSection) holder;
                LinearLayoutManager mLayoutManager;
                mLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
                videos_holder.rv_videos_section.setLayoutManager(mLayoutManager);
                NewsAdapter adapter_videos_section = new NewsAdapter(SplashScreen.newses,mContext,3);
                videos_holder.rv_videos_section.setAdapter(adapter_videos_section);

                videos_holder.tv_moreVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        transaction  = fragmentManager.beginTransaction();
                        transaction.replace(R.id.container, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });

            }
        }
    }

    @Override
    public int getItemCount() {
        return newses.size();
    }
    // ----- one news view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout linearLayout;
        ImageView iv_newsImg, iv_videoNews ;
        TextView tv_newsSource, tv_seenNum, tv_addFav, tv_newsTime, tv_newsTitle ;
        ImageButton btn_share;
        LikeButton btn_read_later;
        public ViewHolder(LinearLayout v,TextView tv_newsSource,TextView tv_seenNum,
                          TextView tv_addFav, TextView tv_newsTime,TextView tv_newsTitle,
                          ImageView iv_newsImg,ImageView iv_videoNews,ImageButton btn_share, LikeButton btn_read_later ) {
            super(v);
            linearLayout = v;
            this.tv_newsSource= tv_newsSource;
            this.tv_seenNum= tv_seenNum;
            this.tv_addFav= tv_addFav;
            this.tv_newsTime= tv_newsTime;
            this.tv_newsTitle = tv_newsTitle ;
            this.iv_newsImg= iv_newsImg;
            this.iv_videoNews = iv_videoNews ;
            this.btn_share = btn_share;
            this.btn_read_later = btn_read_later;
        }
    }

    // ---- one FB native adv view holder
    public static class AdHolder extends RecyclerView.ViewHolder {
        LinearLayout container ;
        MediaView mAdMedia;
        ImageView mAdIcon;
        TextView mAdTitle;
        TextView mAdBody;
        TextView mAdSocialContext;
        Button mAdCallToAction;

        public AdHolder(LinearLayout view ,MediaView mAdMedia,
                        ImageView mAdIcon,
                        TextView mAdTitle,
                        TextView mAdBody,
                        TextView mAdSocialContext,
                        Button mAdCallToAction) {
            super(view);
            this.container = (LinearLayout) view;
            this.mAdMedia = mAdMedia;
            this.mAdTitle = mAdTitle;
            this.mAdBody = mAdBody;
            this.mAdSocialContext = mAdSocialContext;
            this.mAdCallToAction = mAdCallToAction;
            this.mAdIcon = mAdIcon;
        }

    }
    // ---- one google native adv view holder
    public static class ViewHolderAdMob extends RecyclerView.ViewHolder {

        public ViewHolderAdMob(View itemView) {
            super(itemView);
            if(Native_Ads_Advanced.ads_type==1)
                Native_Ads_Advanced.refreshAd(true,
                        false,itemView,mContext);
            else
                Native_Ads_Advanced.refreshAd(false,
                        true,itemView,mContext);
        }
    }
    // -- videos sections view holder --
    public static class ViewHolderVideoSection extends RecyclerView.ViewHolder{
        RecyclerView rv_videos_section;
        public LinearLayout linearLayout;

        TextView tv_moreVideo;
        public ViewHolderVideoSection( LinearLayout linearLayout,RecyclerView rv_videos_section, TextView tv_moreVideo) {
            super(linearLayout);
            this.linearLayout = linearLayout;
            this.rv_videos_section = rv_videos_section;
            this.tv_moreVideo=tv_moreVideo;

        }
    }
    // --- generate FB native ads
    private void genrateFB_ads(RecyclerView.ViewHolder holder){
//        final
    }

    // --- google ads interstitial
    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(Constants.TEST_DEVICE_ID)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    // ---- get item type --
    @Override
    public int getItemViewType(int position) {
        switch (newses.get(position).getNews_id()){
            case "adsPartItem_google":
                return 2;
            case "adsPartItem_fb":
                return 3;
            case "videos_section":
                return 4;
            default:
                return 1;
        }
    }

    // --- async task to get news body fro favourite news --

    private void makeJsonObjectRequest(final String news_id) {

        final ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("حفظ");
        dialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://almogaz.com/api/get_one_new/"+news_id, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    // Parsing json object response
                    String saved_html = response.getString("body_html");
                    ArrayList<String> html_images = new ArrayList<>();
                    html_images = constants.getAllHtmlImages(response.getString("body_html"));
                    if (html_images.size()!=0){
                        for (int i = 0; i<html_images.size(); i++){
                            saved_html = saved_html.replace(html_images.get(i),"file:///data/user/0/almogaz.orangestudio.com/files/"+html_images.get(i).replaceAll("/","_"));
                            Log.e("Images_html ", html_images.get(i));
                        }
                    }
                    if (db_helper.updateNewsDetail(news_id,"fav_news",saved_html,"news_detail"))
                        Toast.makeText(mContext, "تم حفظ الخبر للقراءة لاحقا", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // hide the progress dialog
                dialog.dismiss();
            }
        }){

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("api-key", Constants.API_KEY);
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(50000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
}
