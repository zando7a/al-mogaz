package almogaz.orangestudio.com.Controller;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

import java.util.ArrayList;

import almogaz.orangestudio.com.Model.Videos;
import almogaz.orangestudio.com.R;

/**
 * Created by sameh on 05/07/17.
 */

public class NewsVideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements YouTubePlayer.OnInitializedListener{
    Context context;
    ArrayList<Videos> news_videos = new ArrayList<>();

    public NewsVideosAdapter(ArrayList<Videos> news_videos, Context context) {
        this.news_videos = news_videos;
        this.context = context;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout linearLayout;
        ImageView iv_youTubethumb;
        public ViewHolder(LinearLayout v, ImageView iv_youTubethumb) {
            super(v);
            linearLayout = v;
            this.iv_youTubethumb = iv_youTubethumb;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v =null ;
        RecyclerView.ViewHolder viewHolder = null;
        v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.one_video_for_news, parent, false);

        ImageView iv_youTubethumb;
        iv_youTubethumb = (ImageView) v.findViewById(R.id.iv_youtube_thumb);
        viewHolder = new ViewHolder(v,iv_youTubethumb);

        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        if (news_videos.get(position).getVideo_type().equals("youtube")){
            Glide.with(context)
                    .load("http://img.youtube.com/vi/"+news_videos.get(position).getVideo_id()+"/default.jpg")
                    .into(viewHolder.iv_youTubethumb);
        }
        else if (news_videos.get(position).getVideo_type().equals("facebook")){
            Glide.with(context)
                    .load("https://graph.facebook.com/"+news_videos.get(position).getVideo_id()+"/picture")
                    .into(viewHolder.iv_youTubethumb);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context,R.style.AlerDialogTheme);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View dialogView = inflater.inflate(R.layout.youtube_alert_dialog, null);
                dialogBuilder.setView(dialogView);
                final AlertDialog dialogTest = dialogBuilder.create();
//                dialogTest.setCancelable(false);
                dialogTest.show();
                WebView youTubeView = (WebView) dialogView.findViewById(R.id.youtube_viewer);
                WebSettings webSettings = youTubeView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                webSettings.setLoadWithOverviewMode(true);
                webSettings.setUseWideViewPort(true);
                youTubeView.setInitialScale(1);
//                if (news_videos.get(position).getVideo_type().equals("youtube")){
                    youTubeView.loadData("<iframe width=\"1280\" height=\"720\" src=\"https://www.youtube.com/embed/"+news_videos.get(position).getVideo_id()+"?rel=0\" frameborder=\"0\" allowfullscreen></iframe>","text/html", "UTF-8");
//                }

//                else
//                {
//                    youTubeView.loadData(news_videos.get(position).getVideo_url(),"text/html", "UTF-8");
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return news_videos.size();
    }
}
