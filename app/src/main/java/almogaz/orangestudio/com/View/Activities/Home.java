package almogaz.orangestudio.com.View.Activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;

import java.util.Locale;

import almogaz.orangestudio.com.Controller.SectionsPagerAdapter;
import almogaz.orangestudio.com.Controller.SettingsAdapter;
import almogaz.orangestudio.com.Helper.BottomNavigationViewHelper;
import almogaz.orangestudio.com.Helper.Constants;
import almogaz.orangestudio.com.Helper.DataBaseHelper;
import almogaz.orangestudio.com.Helper.Font;
import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Fragments.LatestNews;
import almogaz.orangestudio.com.View.Fragments.MainCategories;
import almogaz.orangestudio.com.View.Fragments.MyNews;
import almogaz.orangestudio.com.View.Fragments.Videos;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Home extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private FrameLayout mViewPager;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    RecyclerView rv_settings ;
    LinearLayoutManager linearLayoutManager ;
    boolean [] setting_type = {true, false,false,false,false,
            true, false,false,false,false,false,
            true, false,false,false,false,false};

    int mode;
    ImageButton btn_side_menu;
    DrawerLayout m_drawerLayout;

    LatestNews latestNews;
    Videos videos;
    MyNews myNews;
    MainCategories mainCategories;

    MenuItem prevMenuItem;
    private InterstitialAd interstitialAd;
    //    ----
    DataBaseHelper db_helper;
    // --
    private Fragment fragment = new LatestNews();
    private FragmentManager fragmentManager;
    FragmentTransaction transaction ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        -------------- font implementaion -------
        new Font().fontStyle();
        //---------*---------------------
        mode = SettingsAdapter.mode;
        if(mode==0){
            setTheme(R.style.AppTheme);
        }
        else if(mode==1){
            setTheme(R.style.AppTheme1);
        }
        else {
            setTheme(R.style.AppTheme2);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        fragmentManager = getSupportFragmentManager();

        transaction  = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
        fragmentManager.executePendingTransactions();

        db_helper = new DataBaseHelper(this);
        AdSettings.addTestDevice(Constants.TEST_DEVICE_ID);
//        ------------ side menu part -----------------
        rv_settings = (RecyclerView) findViewById(R.id.rv_setting);
        linearLayoutManager = new LinearLayoutManager(this);
        rv_settings.setLayoutManager(linearLayoutManager);
        rv_settings.setHasFixedSize(true);

        SettingsAdapter settingsAdapter = new SettingsAdapter(this,setting_type);
        rv_settings.setAdapter(settingsAdapter);

        //        --- navigation drawer implmentation -----
        btn_side_menu = (ImageButton) findViewById(R.id.btn_side_menu);
        btn_side_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (Locale.getDefault().getDisplayLanguage().equals("English")){
                    if(m_drawerLayout.isDrawerOpen(GravityCompat.END)) {
                        m_drawerLayout.closeDrawer(Gravity.END);
                    }
                    else {
                        m_drawerLayout.openDrawer(Gravity.END);
                    }
                }
                else {
                    if(m_drawerLayout.isDrawerOpen(GravityCompat.START)) {
                        m_drawerLayout.closeDrawer(Gravity.START);
                    }
                    else {
                        m_drawerLayout.openDrawer(Gravity.START);
                    }
                }
            }
        });
//        ----------------------------
        final BottomNavigationView navigationView = (BottomNavigationView)findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);

        BottomNavigationViewHelper.disableShiftMode(navigationView);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));}

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
//        mSectionsPagerAdapter = new SectionsPagerAdapter(this,getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (FrameLayout) findViewById(R.id.container);
//        mViewPager.setAdapter(mSectionsPagerAdapter);
//        mViewPager.setOffscreenPageLimit(2);
        //---------*---------------------
        if(mode == 0){
            mViewPager.setBackgroundColor(ContextCompat.getColor(this, R.color.background_material_light));
            navigationView.setBackgroundColor(ContextCompat.getColor(this, R.color.background_material_light));
        }
        else if(mode == 1){
            mViewPager.setBackgroundColor(ContextCompat.getColor(this, R.color.background_material_light1));
            navigationView.setBackgroundColor(ContextCompat.getColor(this, R.color.background_material_light1));
        }
        else {
            mViewPager.setBackgroundColor(ContextCompat.getColor(this, R.color.background_material_light2));
            navigationView.setBackgroundColor(ContextCompat.getColor(this, R.color.background_material_light2));
        }

//        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                if (prevMenuItem != null) {
//                    prevMenuItem.setChecked(false);
//                }
//                else
//                {
//                    navigationView.getMenu().getItem(0).setChecked(false);
//                }
//                navigationView.getMenu().getItem(position).setChecked(true);
//                prevMenuItem = navigationView.getMenu().getItem(position);
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

        navigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                setSelectedBottomItm(0,navigationView);
                                fragment = new LatestNews();
                                transaction  = fragmentManager.beginTransaction();
                                transaction.replace(R.id.container, fragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                                break;
                            case R.id.action_videoNews:
                                setSelectedBottomItm(1,navigationView);
                                fragment = new Videos();
                                transaction  = fragmentManager.beginTransaction();
                                transaction.replace(R.id.container, fragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                                break;
                            case R.id.action_myNews:
                                setSelectedBottomItm(2,navigationView);
                                fragment = new MyNews();
                                transaction  = fragmentManager.beginTransaction();
                                transaction.replace(R.id.container, fragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                                break;
                            case R.id.action_categories:
                                setSelectedBottomItm(3,navigationView);
                                fragment = new MainCategories();
                                transaction  = fragmentManager.beginTransaction();
                                transaction.replace(R.id.container, fragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                                break;
                        }
                        return false;
                    }
                });

        showAdd();

        //Toast.makeText(this, ""+, Toast.LENGTH_SHORT).show();
    }

    private void setSelectedBottomItm(int position, BottomNavigationView navigationView){
        if (prevMenuItem != null) {
            prevMenuItem.setChecked(false);
        }
        else
        {
            navigationView.getMenu().getItem(0).setChecked(false);
        }
        navigationView.getMenu().getItem(position).setChecked(true);
        prevMenuItem = navigationView.getMenu().getItem(position);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
    private void setupViewPager(ViewPager viewPager)
    {
        mSectionsPagerAdapter = new SectionsPagerAdapter(this,getSupportFragmentManager());
        latestNews=new LatestNews();
        videos=new Videos();
        myNews=new MyNews();
        mainCategories = new MainCategories();
        mSectionsPagerAdapter.addFragment(latestNews);
        mSectionsPagerAdapter.addFragment(videos);
        mSectionsPagerAdapter.addFragment(myNews);
        mSectionsPagerAdapter.addFragment(mainCategories);

        viewPager.setAdapter(mSectionsPagerAdapter);

    }

    private void showAdd(){
        interstitialAd = new InterstitialAd(this, "140879569954442_140880496621016");
        interstitialAd.loadAd();
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e("interstitialAd Error", adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

            @Override
            public void onInterstitialDisplayed(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {

            }
        });
    }
}
