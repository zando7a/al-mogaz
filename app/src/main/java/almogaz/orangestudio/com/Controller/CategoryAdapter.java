package almogaz.orangestudio.com.Controller;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import almogaz.orangestudio.com.Model.Category;
import almogaz.orangestudio.com.R;
import almogaz.orangestudio.com.View.Activities.OneCategoryNewsShow;

/**
 * Created by orange on 05/04/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    ArrayList<Category> categories ;
    Context context ;

    public CategoryAdapter(ArrayList<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tv_categoryName;
        public ViewHolder(TextView tv_categoryName) {
            super(tv_categoryName);
            this.tv_categoryName = tv_categoryName;
        }
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);
        CategoryAdapter.ViewHolder vh = new CategoryAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
        holder.tv_categoryName.setText(categories.get(position).getCategory_name());
        holder.tv_categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OneCategoryNewsShow.CATEGORY_ID = categories.get(position).getCategory_id();
                context.startActivity(new Intent(context,OneCategoryNewsShow.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
